load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/diagnostics_cam.ncl"
load "$NCARG_ROOT/custom_functions_obs.ncl"
load "$NCARG_ROOT/custom_functions_CESM.ncl"

begin
	case 	  = (/"raspe_eul_06","raspe_eul_09","raspe_eul_07","raspe_eul_11","raspe_eul_10"/)
	;case 	  = (/"raspe_eul_06","raspe_eul_09"/)
	
	dir = "/maloney-scratch/whannah/CESM/data/"+case+"/budget/"
	
	lat1 = -10.
	lat2 =  10.
	
	nRunAvg = 17
	
	fig_type  = "png"
	
	colors = (/"blue","cyan","red","orange","green"/)
	
;====================================================================================================
;====================================================================================================
	case_name = (/"Control","Tok","DeepA","DeepA+Tok","DeepB","ERAi"/)
	num_c = dimsizes(case)
	fig_file = "MSE_budget_figures/MSE_budget.terms.eq.climo"
	
	lat = LoadCESMlat("raspe_eul_06",lat1,lat2)
	lon = LoadCESMlon("raspe_eul_06",0.,360.)
	;lat = LoadERAilat(lat1,lat2)
	;lon = LoadERAilon(0.,360.)
	num_lat = dimsizes(lat)
	num_lon = dimsizes(lon)
	num_t = 10*365
;====================================================================================================
;====================================================================================================
	res = True
	res@gsnDraw  = False
	res@gsnFrame = False
	res@gsnRightStringFontHeightF  = 0.015
	res@gsnLeftStringFontHeightF   = 0.015
	res@gsnCenterStringFontHeightF = 0.015
	res@gsnCenterString = ""
	res@gsnRightString  = ""
	res@gsnLeftString   = ""
	res@tmXBLabelFontHeightF = 0.015
	res@tmYLLabelFontHeightF = 0.015
	res@tmXBMinorLengthF = -0.005
	res@tmXBMajorLengthF = -0.01
	res@tmXTMinorLengthF = -0.005
	res@tmXTMajorLengthF = -0.01
	res@trXMaxF = 360.
	res@vpHeightF = 0.2
	res@xyDashPattern 	= 0
	res@xyLineThicknessF 	= 2.
	res@tiYAxisString = "kg W m~S~-2"

	lres = res
	lres@xyDashPattern 	= 1
	lres@xyLineThicknessF 	= 1.
	lres@xyLineColor 	= "black"

		res@xyLineColors = colors
		res@pmLegendOrthogonalPosF = -1.5
	  	res@pmLegendParallelPosF   =  0.35
	  	res@pmLegendWidthF         =  0.05
	  	res@pmLegendHeightF        =  0.08    
	  	;res@lgBoxMinorExtentF      = 0.1      
	  	res@lgLabelFontHeightF     = 0.01 
	  	res@xyExplicitLegendLabels = case_name  
;====================================================================================================
;====================================================================================================
	MSEDT = new((/num_c,num_t,num_lon/),float)
	delVH = new((/num_c,num_t,num_lon/),float)
	VdelH = new((/num_c,num_t,num_lon/),float)
	HdelV = new((/num_c,num_t,num_lon/),float)
	dHWdp = new((/num_c,num_t,num_lon/),float)
	WdHdp = new((/num_c,num_t,num_lon/),float)
	HdWdp = new((/num_c,num_t,num_lon/),float)
	COLQR = new((/num_c,num_t,num_lon/),float)
	LHFLX = new((/num_c,num_t,num_lon/),float)
	SHFLX = new((/num_c,num_t,num_lon/),float)
	
	MSEDT!0 = "case"
  	MSEDT!1 = "time"
  	MSEDT!2 = "lon"
  	MSEDT&lon = lon
  	
  	copy_VarCoords(MSEDT,delVH)
  	copy_VarCoords(MSEDT,VdelH)
  	copy_VarCoords(MSEDT,HdelV)
  	copy_VarCoords(MSEDT,dHWdp)
  	copy_VarCoords(MSEDT,WdHdp)
  	copy_VarCoords(MSEDT,HdWdp)
  	copy_VarCoords(MSEDT,COLQR)
  	copy_VarCoords(MSEDT,LHFLX)
  	copy_VarCoords(MSEDT,SHFLX)
;====================================================================================================
;====================================================================================================
do c = 0,num_c-1	
    if case(c).eq."ERAi" then
       yr1 = 0
       yr2 = 9
     else
       yr1 = 2
       yr2 = 8      
     end if  
    
    ;-------------------------------------------------------------
    ; Total Tendency
    var = "MSEDT"
    infile = addfile(dir(c)+case(c)+".MSE.budget."+var+"."+yr1+"-"+yr2+".nc","r")
    num_t = dimsizes(infile->$var$(:,0,0))
    MSEDT(c,:num_t-1,:) = dim_avg_n(infile->$var$(:,{lat1:lat2},:),1)
    ;-------------------------------------------------------------
    ; Advection/Convergence terms (flux form)
    var = "delVH"
    infile = addfile(dir(c)+case(c)+".MSE.budget."+var+"."+yr1+"-"+yr2+".nc","r")
    delVH(c,:num_t-1,:) = dim_avg_n(infile->$var$(:,{lat1:lat2},:),1)
    var = "dHWdp"
    infile = addfile(dir(c)+case(c)+".MSE.budget."+var+"."+yr1+"-"+yr2+".nc","r")
    dHWdp(c,:num_t-1,:) = dim_avg_n(infile->$var$(:,{lat1:lat2},:),1)
    ;-------------------------------------------------------------
    ; Advection/Convergence terms (advective form)
    var = "VdelH"
    infile = addfile(dir(c)+case(c)+".MSE.budget."+var+"."+yr1+"-"+yr2+".nc","r")
    VdelH(c,:num_t-1,:) = dim_avg_n(infile->$var$(:,{lat1:lat2},:),1)
    var = "WdHdp"
    infile = addfile(dir(c)+case(c)+".MSE.budget."+var+"."+yr1+"-"+yr2+".nc","r")
    WdHdp(c,:num_t-1,:) = dim_avg_n(infile->$var$(:,{lat1:lat2},:),1)
    ;-------------------------------------------------------------
    ; RHS Forcing terms
    var = "COLQR"
    infile = addfile(dir(c)+case(c)+".MSE.budget."+var+"."+yr1+"-"+yr2+".nc","r")
    COLQR(c,:num_t-1,:) = dim_avg_n(infile->$var$(:,{lat1:lat2},:),1)
    var = "LHFLX"
    infile = addfile(dir(c)+case(c)+".MSE.budget."+var+"."+yr1+"-"+yr2+".nc","r")
    LHFLX(c,:num_t-1,:) = dim_avg_n(infile->$var$(:,{lat1:lat2},:),1)
    var = "SHFLX"
    infile = addfile(dir(c)+case(c)+".MSE.budget."+var+"."+yr1+"-"+yr2+".nc","r")
    SHFLX(c,:num_t-1,:) = dim_avg_n(infile->$var$(:,{lat1:lat2},:),1)
    ;-------------------------------------------------------------
    ;-------------------------------------------------------------
    ;FF = MSEDT + delVH + dHWdp - F
    ;AF = MSEDT + VdelH + WdHdp - F
    ;num_t = dimsizes(FF(:,0,0))
    ;RFF(c,:num_t-1,:) = dim_avg_n(FF,1)
    ;RAF(c,:num_t-1,:) = dim_avg_n(AF,1)
    ;-------------------------------------------------------------
    ;-------------------------------------------------------------
      ;delete([/MSEDT,delVH,dHWdp,VdelH,WdHdp,F/])
  end do
;====================================================================================================
;====================================================================================================
  wks = gsn_open_wks(fig_type,fig_file)
  plot = new(9,graphic)
  ;wks@wkPaperWidthF = 6.
  	
  	res@pmLegendDisplayMode    = "Never" 
  	res@tmXBLabelsOn  = False
  	restop = res
  	resbot = res
  	resbot@tmXBLabelsOn  = True
	restop@pmLegendDisplayMode    = "Always" 
	res@pmLegendOrthogonalPosF = -1.
	
	restop@gsnRightString = "Total Tendency"
  plot(0) = gsn_csm_xy(wks,lon,dim_avg_n_Wrap(MSEDT,1),restop)
	res@gsnRightString = "Advective Terms (flux form)"
  plot(1) = gsn_csm_xy(wks,lon,dim_avg_n_Wrap(delVH+dHWdp,1),res)
  	res@gsnRightString = "Advective Terms (adv. form)"
  plot(2) = gsn_csm_xy(wks,lon,dim_avg_n_Wrap(VdelH+WdHdp,1),res)

  	res@gsnRightString = "Radiation"
  plot(3) = gsn_csm_xy(wks,lon,dim_avg_n_Wrap(COLQR,1),res)
  	res@gsnRightString = "delVH"
  plot(4) = gsn_csm_xy(wks,lon,dim_avg_n_Wrap(delVH,1),res)
  	res@gsnRightString = "VdelH"
  plot(5) = gsn_csm_xy(wks,lon,dim_avg_n_Wrap(VdelH,1),res)

  	resbot@gsnRightString = "Surface Fluxes"
  plot(6) = gsn_csm_xy(wks,lon,dim_avg_n_Wrap(LHFLX+SHFLX,1),resbot)
  	resbot@gsnRightString = "dHWdp"
  plot(7) = gsn_csm_xy(wks,lon,dim_avg_n_Wrap(dHWdp,1),resbot)	
  	resbot@gsnRightString = "WdHdp"
  plot(8) = gsn_csm_xy(wks,lon,dim_avg_n_Wrap(WdHdp,1),resbot)

  do p = 0,dimsizes(plot)-1
    overlay(plot(p),gsn_csm_xy(wks,lon,lon*0,lres))
  end do
;====================================================================================================
;====================================================================================================
  	pres = True
  	pres@gsnPanelFigureStrings 	= (/"a","b","c","d","e","f"/) ; add strings to panel
  	pres@amJust   		   	= "TopLeft"
	pres@gsnPanelBottom 		= 0.05
	pres@gsnPanelFigureStringsFontHeightF	= 0.015
	pres@amJust   = "TopLeft"
	
  
  gsn_panel(wks,plot,(/3,3/),pres)

	print("")
	print(fig_file+"."+fig_type)
	print("")
	
;====================================================================================================
;====================================================================================================
  ;delete([/lat,lon,res,plot1,plot2/])
end


