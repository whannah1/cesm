load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_CESM.ncl"

begin

	case = (/"raspe_eul_06","raspe_eul_07","raspe_eul_09","raspe_eul_10","raspe_eul_11"/)
	;case = (/"raspe_eul_09","raspe_eul_10","raspe_eul_11"/)


	dir  = "/maloney-scratch/whannah/CESM/data/"+case+"/raw_data/"
	
	odir = "/maloney-scratch/whannah/CESM/data/"+case+"/budget/"
	
	lat1 = -30.
	lat2 =  30.

	top_lev = 100.
	
	yr1 = 2
	yr2 = 8
	
	cpd 	= 1004.		; J / (kg K)
	cpv 	= 1850.		; J / (kg K)
	Tf	= 273.15 	; K
	g  	= 9.81		; m / s^2
	Lv  	= 2.5*10.^6.	; J / kg
	;Po	= 100000.	; Pa
	Rd	= 287.		; J / (kg K)
	Rv	= 461.		; J / (kg K)
	esf	= 611.		; Pa
	eps	= 0.622		; (epsilon)

	ifile_stub = case+".cam2.h1."
	
	ofile_stub = case+".S.budget."
	
	num_c = dimsizes(case)

;====================================================================================================
;====================================================================================================
    filesize = 73
    
do c = 0,num_c-1
    
    files = systemfunc("ls "+dir(c)+case(c)+".cam2.h1.000["+yr1+"-"+yr2+"]*")
    num_files = dimsizes(files)
    infile = addfile(files(0),"r")
    ;--------------------------------------
    ; coordinate variables
    ;--------------------------------------
      dims = dimsizes(dimsizes(infile->T))
      lat = infile->lat({lat1:lat2})
      lon = infile->lon
      lev = infile->lev({top_lev:})          
        print("")
        print("  case: "+case(c))
        print("    lat = 	"+sprintf("%6.4g",min(lat))+"	:	"+sprintf("%6.4g",max(lat)))
        print("    lon = 	"+sprintf("%6.4g",min(lon))+"	:	"+sprintf("%6.4g",max(lon)))
        print("    lev = 	"+max(lev)+"	:	"+min(lev))
        print("")
      latsz = dimsizes(lat)
      lonsz = dimsizes(lon)
      levsz = dimsizes(lev)
      itop_lev = ind(infile->lev.eq.infile->lev({top_lev}))
      global_lat   = infile->lat
      global_latsz = dimsizes(global_lat)
;====================================================================================================
;====================================================================================================
  do f = 0,num_files-1
    infile = addfile(files(f),"r")
    print("      ifile > "+files(f))    
    ;***********************************************************************************
    ;***********************************************************************************
    do t = 0,filesize-1 
      ;-----------------------------------------------------------   
      ;-----------------------------------------------------------       
        T = infile->T (t,{top_lev:},:,:)
        Z = infile->Z3(t,{top_lev:},:,:)
        Q = infile->Q (t,{top_lev:},:,:)
        U = infile->U(t,{top_lev:},:,:)
        V = infile->V(t,{top_lev:},:,:)
        hyam = conform(T,infile->hyam({top_lev:}),0)
        hybm = conform(T,infile->hybm({top_lev:}),(/0/))
        PS   = conform(T,infile->PS(t,:,:),(/1,2/))
        Po   = infile->P0 
        P    = tofloat( hyam*Po + hybm*PS )
        LHFLX = infile->LHFLX(t,{lat1:lat2},:) 
        SHFLX = infile->SHFLX(t,{lat1:lat2},:) 
        QR = infile->QRS(t,{top_lev:},{lat1:lat2},:) + infile->QRL(t,{top_lev:},{lat1:lat2},:)
        
        Pv = calc_vapor_pressure(Q,P)
        Pd = P-Pv
        
        ; Simplified equation from David Raymond's Thermo notes
        S =  (cpd+Q*cpv)*log(T/Tf) - Rd*log(Pd/Po) - Q*Rv*log(Pv/esf) + Lv*Q/Tf
        
        	S!0 = "lev"
        	S!1 = "lat"
        	S!2 = "lon"
        	S&lev = lev
        	S&lat = global_lat
        	S&lon = lon
        	
        	delete([/T,Z,Q,P,Pv,Pd,hyam,hybm,PS/])
        	
        	copy_VarCoords(S,U)
        	copy_VarCoords(S,V)
      ;-----------------------------------------------------------
      ; Divergence
      ;-----------------------------------------------------------
        DIV = uv2dvG_Wrap(U,V)
        tDIV = DIV(:,{lat1:lat2},:)
          delete(DIV)
        DIV = tDIV
          delete(tDIV)
        ;tU = U(:,{lat1:lat2},:)
        ;tV = V(:,{lat1:lat2},:)
        ;  delete([/U,V/])
        ;U = tU
        ;V = tV
        ;  delete([/tU,tV/])
      ;-----------------------------------------------------------   
      ; Pressure thickness
      ;-----------------------------------------------------------   
        dP = new((/levsz,latsz,lonsz/),float)
        PS = infile->PS(t,{lat1:lat2},:)
	Po = infile->P0          
        hyai = infile->hyai(itop_lev:)
        hybi = infile->hybi(itop_lev:)
        do k = 0,levsz-1
          Pk1 = tofloat(hyai(k  )*Po+hybi(k  )*PS)
          Pk2 = tofloat(hyai(k+1)*Po+hybi(k+1)*PS)
          dP(k,:,:) = Pk2-Pk1 
        end do         
          delete([/PS,hyai,hybi,Pk1,Pk2/])
      ;-----------------------------------------------------------   
      ; Calculate horizontal gradients
      ;-----------------------------------------------------------   
          S_dx = new((/levsz,global_latsz,lonsz/),float)
          S_dy = new((/levsz,global_latsz,lonsz/),float)
          SU_dx = new((/levsz,global_latsz,lonsz/),float)
          SU_dy = new((/levsz,global_latsz,lonsz/),float)
          SV_dx = new((/levsz,global_latsz,lonsz/),float)
          SV_dy = new((/levsz,global_latsz,lonsz/),float)
        gradsg(S,S_dx,S_dy)
        gradsg(S,S_dx,S_dy)
        gradsg(S*U,SU_dx,SU_dy)
        gradsg(S*U,SU_dx,SU_dy)
        gradsg(S*V,SV_dx,SV_dy)
        gradsg(S*V,SV_dx,SV_dy)
          copy_VarCoords(S,S_dx)
          copy_VarCoords(S,S_dy)
          copy_VarCoords(S,SU_dx)
          copy_VarCoords(S,SU_dy)
          copy_VarCoords(S,SV_dx)
          copy_VarCoords(S,SV_dy)
        S_grad_x = S_dx(:,{lat1:lat2},:)
        S_grad_y = S_dy(:,{lat1:lat2},:)
        SU_grad_x = S_dx(:,{lat1:lat2},:)
        ;SU_grad_y = S_dy(:,{lat1:lat2},:)
        ;SV_grad_x = S_dx(:,{lat1:lat2},:)
        SV_grad_y = S_dy(:,{lat1:lat2},:)
        tS = S(:,{lat1:lat2},:)
          delete(S)
        S = tS
          delete([/U,V,tS,S_dx,S_dy,SU_dx,SU_dy,SV_dx,SV_dy/])
      ;-----------------------------------------------------------   
      ; Total tendency
      ;-----------------------------------------------------------   
        if (f.eq.0).and.(t.eq.0) then
          STEND = 0.
          S_old = S
        else
          STEND = ( S - S_old ) / ( 2*24*60*60 )
          S_old = S
        end if
        SDT  = dim_sum_n(STEND*dP/g,0)
        Svi  = dim_sum_n(S    *dP/g,0)
          delete([/STEND/])      
      ;-----------------------------------------------------------
      ; Radiative heating
      ;-----------------------------------------------------------
        COLQR = dim_sum_n(QR*cp*dP/g,0)
          delete(QR)
      ;-----------------------------------------------------------
      ; Horizontal advective tendency
      ;-----------------------------------------------------------
        HS = dim_sum_n(-(U*S_grad_x+V*S_grad_y)*dP/g,0)
          delete([/S_grad_x,S_grad_y,U,V/])
      ;-----------------------------------------------------------
      ; Verticla advective tendency
      ;-----------------------------------------------------------
        ;VQE = SDT - HSE - SFLX - SHFLX - COLQR
        VS = dim_sum_n(-DIV*S*dP/g,0)
      ;-----------------------------------------------------------
      ; Precipitation
      ;-----------------------------------------------------------      
      ;  PRECT = ( infile->PRECC(t,{lat1:lat2},:) + infile->PRECL(t,{lat1:lat2},:) )*1000.
      ;-----------------------------------------------------------
      ; residual
      ;-----------------------------------------------------------
        residual = SDT - HS - VS - LHFLX - SHFLX - COLQR
      ;-----------------------------------------------------------
      ;-----------------------------------------------------------
        S!0 = "lev"
        S!1 = "lat"
        S!2 = "lon"
        S&lev = lev
        S&lat = lat
        S&lon = lon
        	copy_VarCoords(S,dP)
        	copy_VarCoords(S,DIV)
        SDT!0 = "lat"
        SDT!1 = "lon"
        SDT&lat = lat
        SDT&lon = lon
        	copy_VarCoords(SDT,VS)
        	copy_VarCoords(SDT,HS)
        	copy_VarCoords(SDT,COLQR)
        	copy_VarCoords(SDT,LHFLX)
        	copy_VarCoords(SDT,SHFLX)
        	;copy_VarCoords(SDT,PRECT)
        
        unit_str = "kg/s/m^2"
        SDT@long_name = "Column S Tendency"
        VS@long_name 	= "Vert. advective S tend."
        HS@long_name 	= "Horz. advective S tend."
        COLQR@long_name = "Column Radiate Heating"
        LHFLX@long_name = "Sfc LH Flux"
        SHFLX@long_name = "Sfc SH Flux"
        ;PRECT@long_name = "Total Precip"
        SDT@units	= unit_str
        VS@units	= unit_str
        HS@units	= unit_str
        COLQR@units	= unit_str
        LHFLX@units	= unit_str
        SHFLX@units	= unit_str
        ;PRECT@units	= unit_str
      ;----------------------------------------------------------
      ; Create output files
      ;-----------------------------------------------------------
      ovar 	= (/"SDT","VS","HS","COLQR","LHFLX","SHFLX","residual"/)
      long_name = (/"Column S Tendency","Vert. advective S tend.","Horz. advective S tend.","Column Radiate Heating","Sfc LH Flux","Sfc SH Flux","residual"/)
      units     = "kg/s/m^2"
      num_v = dimsizes(ovar)
      oV = new((/num_v,latsz,lonsz/),float)
      oV(0,:,:) = SDT
      oV(1,:,:) = VS
      oV(2,:,:) = HS
      oV(3,:,:) = COLQR
      oV(4,:,:) = LHFLX
      oV(5,:,:) = SHFLX
      oV(6,:,:) = (/ residual /)
      
      if (f.eq.0).and.(t.eq.0) then
          print("")
          print("		creating dummy file variable...")
          num_t = (yr2-yr1+1)*365
          	fvar = new((/num_t,latsz,lonsz/),float)
          	fvar!0 = "time"
          	fvar!1 = "lat"
          	fvar!2 = "lon"
          	fvar&lat = lat
          	fvar&lon = lon
        ofile = odir(c)+ofile_stub(c)+ovar+"."+yr1+"-"+yr2+".nc"
        do v = 0,num_v-1
        if .not.isfilepresent(ofile(v)) then
          ;system("rm "+ofile(v)) 
          print("		creating file #"+(v+1)+"	"+ofile(v))
          fvar@long_name = long_name(v)
          fvar@units = units
          outfile = addfile(ofile(v),"c")
          outfile->$ovar(v)$ = fvar
        end if
        end do
        if isvar("fvar") then
          delete(fvar)
        end if
        
        print("")
      end if
      
      do v = 0,num_v-1
        outfile = addfile(ofile(v),"w")
        outfile->$ovar(v)$(t,:,:) = (/ oV(v,:,:) /)
      end do
      ;-----------------------------------------------------------
      ;-----------------------------------------------------------
        delete([/Z,Q,S,DIV/])
    end do  
    ;***********************************************************************************
    ;***********************************************************************************
    ;end if
  end do
  delete([/files/])
end do
;====================================================================================================
;====================================================================================================
end

