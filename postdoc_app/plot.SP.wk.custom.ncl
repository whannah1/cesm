load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/diagnostics_cam.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/kf_filter.ncl"
begin 
	idir = "/Users/whannah/CESM/code_PD/"
	
	var = "PRECT"
	
	fig_type = "x11"
	fig_file = idir+"wk.custom"
	
	case = (/"TRMM","SPctl","SP4x"/)
	
	case_name = case
	
	lat1 = -10.
	lat2 =  10.
	
	;SPEC_VAR = "FIG_3_SYM"
	SPEC_VAR = "FIG_3_ASYM"
;=========================================================================================================
; Load pre-calculated spectra
;=========================================================================================================
  num_c = dimsizes(case)
  num_v = dimsizes(var)
  
  spec_RED = new((/num_c,49,31/),float)
  spec_SYM = new((/num_c,49,31/),float)

  ;infile = addfile("/data/whannah/obs/TRMM/SpaceTime.TRMM.nc","r")
  infile = addfile(idir+"SpaceTime.TRMM.nc","r")
  spec_SYM(0,:,:) = infile->$SPEC_VAR$

  infile = addfile(idir+"SpaceTime."+case(1)+"."+var+"."+lat1+"_"+lat2+".nc","r")
  spec_SYM(1,:,:) = infile->$SPEC_VAR$
  
  infile = addfile(idir+"SpaceTime."+case(2)+"."+var+"."+lat1+"_"+lat2+".nc","r")
  spec_SYM(2,:,:) = infile->$SPEC_VAR$
  
  ;spec_SYM = spec_RED/spec_SYM

  
  rlat           = 0.0
  ;Ahe            = (/125.,50.,25.,12.,6./)
  Ahe            = (/80.,50.,25.,12.,6./)
  nWaveType      = 6
  nPlanetaryWave = 50
  nEquivDepth    = dimsizes(Ahe)
  Apzwn          = new((/nWaveType,nEquivDepth,nPlanetaryWave/),"double",1e20)
  Afreq          = Apzwn
  genDispersionCurves(nWaveType, nEquivDepth, nPlanetaryWave, rlat, Ahe, Afreq, Apzwn )

;====================================================================================================
; Create Plot
;====================================================================================================
  wks = gsn_open_wks(fig_type,fig_file)
  plot = new(num_c,graphic)
  gsn_define_colormap(wks,"precip2_17lev")
  ;gsn_define_colormap(wks,"mch_default")
  
  dumdcs     = new ( (/num_c,25/), "graphic") ; dispersion curves symmetric (dcs)
  dummk = new(num_c,graphic)
  
  	res = True
  	res@gsnDraw 				= False
  	res@gsnFrame 			= False
  	res@cnLinesOn 			= False
  	res@cnFillOn 			= True
  	res@gsnSpreadColors 		= True
  	res@lbLabelAngleF 		= -45.
  	res@cnInfoLabelOn 		= False
  	res@cnLineLabelsOn 		= False
  	res@gsnLeftStringFontHeightF = 0.03
  	res@vpHeightF = 0.45
  	
  	res@lbLabelBarOn 	= False
  	
  		dcres = True
   		dcres@gsLineThicknessF  = 1.0
   		dcres@gsLineDashPattern = 0
   		
   		dcres_dash = dcres
   		dcres_dash@gsLineDashPattern = 1
	
   		txres = True
   		txres@txPerimOn     = True
   		txres@txFontHeightF = 0.013
   		txres@txBackgroundFillColor = "Background"
  	
  	
  	res@gsnSpreadColorStart = 4
  	
	 res@cnLevelSelectionMode = "ExplicitLevels"
	 ;if SPEC_VAR.eq."FIG_1_SYM"  then res@cnLevels  =  fspan(-17.,-15.,11) end if
	 ;if SPEC_VAR.eq."FIG_1_ASYM" then res@cnLevels  =  fspan(.8,1.8,11) end if
	 if SPEC_VAR.eq."FIG_3_SYM"  then res@cnLevels  =  fspan(.8,1.8,11) end if
	 if SPEC_VAR.eq."FIG_3_ASYM" then res@cnLevels  =  fspan(.5,1.3,11) end if
	 
  	
    tMin = 2.5
    tMax = 20
    kMin = 1
    kMax = 14
    hMin = 8
    hMax = 200
    
    fMin = 1./tMax
    fMax = 1./tMin
    
    gt = 9.81*(60.*60.*24.)^2.
    g  = 9.81
    circ = (40075.*1000.)
    
    	lres = True
    	lres@gsLineThicknessF  = 2.0
    
    x0 = (/kMin,circ*1./(tMax*(60.*60.*24.))/(sqrt(g*hMin))/)
    y0 = (/1./tMax,1./tMax/)
    	gsnPanelBottom = 0.05
    x1 = (/circ*1./(tMax*(60.*60.*24.))/(sqrt(g*hMin)),kMax/)
    y1 = (x1/circ)*sqrt(gt*hMin) 
    
    x2 = (/kMax,kMax/)
    y2 = (/kMax/circ*sqrt(gt*hMin),1./tMin/) 
    
    x3 = (/kMax,circ*1./(tMin*(60.*60.*24.))/(sqrt(g*hMax))/)
    y3 = (/1./tMin,1./tMin/)

    x4 = (/circ*1./(tMin*(60.*60.*24.))/(sqrt(g*hMax)),kMin/)
    y4 = (x4/circ)*sqrt(gt*hMax)

    x5 = (/kMin,kMin/)
    y5 = (/kMin/circ*sqrt(gt*hMax),1./tMax/)
    
    slp1 = (y1(1)-y1(0))/(x1(1)-x1(0))
    slp4 = (y4(1)-y4(0))/(x4(1)-x4(0))
    
	 
	KWS = spec_SYM
	freq = KWS&freq
	wave = KWS&wave   
	dom  = new(dimsizes(KWS(0,:,:)),float)
	xx = conform(dom,fspan(min(wave),max(wave),dimsizes(wave)),1)
	yy = conform(dom,fspan(min(freq),max(freq),dimsizes(freq)),0)
	ax = new(num_c,float)
	ay = new(num_c,float)
	do c = 0,num_c-1
	  KWS(c,:,:) = where((xx.ge.kMin).and.(xx.le.kMax)      ,KWS(c,:,:),KWS@_FillValue)
	  KWS(c,:,:) = where((yy.ge.fMin).and.(yy.le.fMax)      ,KWS(c,:,:),KWS@_FillValue)
	  KWS(c,:,:) = where((yy.ge.xx*slp1).and.(yy.le.xx*slp4),KWS(c,:,:),KWS@_FillValue)
	  wgt = abs(KWS(c,:,:)) / sum( abs(KWS(c,:,:)) )
	  ax(c) = sum(xx*wgt)
	  ay(c) = sum(yy*wgt)
	end do

	print(ax+"	"+ay)

;spec_SYM = (/KWS/)

  	
  do c = 0,num_c-1
  	res@gsnLeftString = case_name(c)
  	if any(c.eq.(/0,2,4/)) then
  	  res@tiYAxisString = "days~S~-1"
  	else
  	  res@tiYAxisString = ""
  	end if
  	if any(c.eq.(/4,5/)) then
  	  res@tiXAxisString = "Zonal Wavenumber"
  	else
  	  ;res@tiXAxisString = ""
  	end if
  	res@gsnRightString 	= var
    plot(c) = gsn_csm_contour(wks,spec_SYM(c,:,:),res)
    	
   if SPEC_VAR.eq."FIG_3_SYM"  then
  	; Rossby dispersion
  	;dumdcs(c, 0) = gsn_add_polyline(wks,plot(c),Apzwn(3,0,:),Afreq(3,0,:),dcres)
  	dumdcs(c, 1) = gsn_add_polyline(wks,plot(c),Apzwn(3,1,:),Afreq(3,1,:),dcres)
  	dumdcs(c, 2) = gsn_add_polyline(wks,plot(c),Apzwn(3,2,:),Afreq(3,2,:),dcres)
  	dumdcs(c, 3) = gsn_add_polyline(wks,plot(c),Apzwn(3,2,:),Afreq(3,3,:),dcres)
  	;dumdcs(c, 4) = gsn_add_polyline(wks,plot(c),Apzwn(3,2,:),Afreq(3,4,:),dcres)
  	; Kelvin dispersion curves
  	dcres_alt = dcres
  	dcres_alt@gsLineDashPattern = 1
  	;dumdcs(c, 5) = gsn_add_polyline(wks,plot(c),Apzwn(4,0,:),Afreq(4,0,:),dcres_alt)
  	dumdcs(c, 6) = gsn_add_polyline(wks,plot(c),Apzwn(4,1,:),Afreq(4,1,:),dcres)
  	dumdcs(c, 7) = gsn_add_polyline(wks,plot(c),Apzwn(4,2,:),Afreq(4,2,:),dcres)
  	dumdcs(c, 8) = gsn_add_polyline(wks,plot(c),Apzwn(4,2,:),Afreq(4,3,:),dcres)
  	;dumdcs(c, 9) = gsn_add_polyline(wks,plot(c),Apzwn(4,2,:),Afreq(4,4,:),dcres)
  	; 
  	;dumdcs(c,10) = gsn_add_polyline(wks,plot(c),Apzwn(5,0,:),Afreq(5,0,:),dcres)
  	dumdcs(c,11) = gsn_add_polyline(wks,plot(c),Apzwn(5,1,:),Afreq(5,1,:),dcres)
  	dumdcs(c,12) = gsn_add_polyline(wks,plot(c),Apzwn(5,2,:),Afreq(5,2,:),dcres)
  	dumdcs(c,13) = gsn_add_polyline(wks,plot(c),Apzwn(5,2,:),Afreq(5,3,:),dcres)
  	;dumdcs(c,14) = gsn_add_polyline(wks,plot(c),Apzwn(5,2,:),Afreq(5,4,:),dcres)
  	
  	;dumdcs(c, 9) = gsn_add_text(wks,plot(c),"Kelvin",11.5,.40,txres)
  	;dumdcs(c,10) = gsn_add_text(wks,plot(c),"n=1 ER",-10.7,.07,txres)
  	;dumdcs(c,11) = gsn_add_text(wks,plot(c),"n=1 IG",-3.0,.45,txres)
  	;dumdcs(c,12) = gsn_add_text(wks,plot(c),"h=50",-14.0,.78,txres)
  	;dumdcs(c,13) = gsn_add_text(wks,plot(c),"h=25",-14.0,.60,txres)
  	;dumdcs(c,14) = gsn_add_text(wks,plot(c),"h=12",-14.0,.46,txres)
 
    dumdcs(c, 15) = gsn_add_polyline(wks,plot(c),x0,y0,lres)
    dumdcs(c, 16) = gsn_add_polyline(wks,plot(c),x1,y1,lres)
    dumdcs(c, 17) = gsn_add_polyline(wks,plot(c),x2,y2,lres)
    dumdcs(c, 18) = gsn_add_polyline(wks,plot(c),x3,y3,lres)
    dumdcs(c, 19) = gsn_add_polyline(wks,plot(c),x4,y4,lres)
    dumdcs(c, 20) = gsn_add_polyline(wks,plot(c),x5,y5,lres)
  end if
  
  if SPEC_VAR.eq."FIG_3_ASYM"  then
  
  end if
    
    	pmres = True
    	pmres@gsMarkerColor = "red"
    	pmres@gsMarkerIndex = 15
    
  end do
;====================================================================================================
;====================================================================================================

	pres = True
	if .not.res@lbLabelBarOn then pres@gsnPanelLabelBar = True end if
	pres@lbLabelAngleF = -45.
	pres@lbTitleString 	= ""
	pres@lbTitlePosition	= "bottom"
	pres@lbTitleFontHeightF = 0.015
	pres@lbTopMarginF 	=  0.4
	pres@lbBottomMarginF 	= -0.4
	pres@lbLabelFontHeightF = 0.015
	pres@gsnPanelBottom 	= 0.08
	
	pres@gsnPanelFigureStrings 		= (/"a","b","c","d","e","f"/)
	pres@gsnPanelFigureStringsFontHeightF	= 0.015
	pres@amJust   = "TopLeft"
	
	;pres@txString = ivar
	
  gsn_panel(wks,plot,(/2,2/),pres)
  
  	print("")
  	print("	"+fig_file+"."+fig_type)
  	print("")
;====================================================================================================
;====================================================================================================
end

