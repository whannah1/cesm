load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/diagnostics_cam.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/kf_filter.ncl"
load "$NCARG_ROOT/custom_functions_obs.ncl"

begin 

	case = (/"TRMM"/)
	
	case_name = (/"Control","DeepA","Tok","DeepB","DeepA+Tok","TRMM"/)
	
	idir = "/maloney-scratch/whannah/CESM/data/"+case+"/"
	
	var = (/"Q"/)
	;var = (/"U","OMEGA"/)
	
	num_c = dimsizes(case)
  	num_v = dimsizes(var)

	lat1 = -10.
	lat2 =  10.
	
	xlon = 150.
	
	lon1 = xlon;0.
	lon2 = xlon;360.
	
	yr1 = 0
	yr2 = 9
	
	mxlag = 15

		tMin = 2.5
  		tMax = 20
  		kMin = 1
  		kMax = 14
  		hMin = 8
  		hMax = 200
  		waveName = "Kelvin"


;=========================================================================================================
; Load data
;=========================================================================================================

  num_t = (yr2-yr1+1)*365
  
  time = ispan(0,(yr2-yr1+1)*365-1,1)
  time@units = "days since Jan. 1, 2000"

  lev  = LoadERAilev()
  lat  = LoadERAilat(lat1,lat2)
  lon  = LoadERAilon(lon1,lon2)
  
  num_lev = dimsizes(lev)
  num_lat = dimsizes(lat)
  num_lon = dimsizes(lon)

do c = 0,num_c-1  
  ;====================================================================================================
  ; Create KW index
  ;====================================================================================================
;if False then
;  obsPerDay = 1
;  index = new((/num_t,num_lon/),float)
;  indV = (/ dim_avg_n( LoadTRMMdata_interp(yr1,yr2,-5.,5.,0.,360.,lat,lon) ,(/1/))  /)
;    indV!0 = "time"
;    indV!1 = "lon"
;    indV&time = time
;    indV&lon = lon
;  index(:,:) = kf_filter(indV,obsPerDay,tMin,tMax,kMin,kMax,hMin,hMax,waveName)
;  do i = 0,num_lon-1
;    index(:,i) = index(:,i)/stddev(index(:,i))
;  end do
;    delete(indV)
;  ;====================================================================================================
;  ; Write index to file
;  ;====================================================================================================
;  ofile = idir(c)+"lagreg.vert."+waveName+".hmax"+hMax+".TRMM_index.nc"
;  if isfilepresent(ofile) then
;    system("rm "+ofile)
;  end if
;  outfile = addfile(ofile,"c")
;  outfile->index = index
;    delete([/index/])horz
;end if
  
  ifile = idir(c)+"lagreg_alt.horz."+waveName+".hmax"+hMax+".TRMM_index.nc"
  infile = addfile(ifile,"r")
  index = infile->index
  ;====================================================================================================
  ; screen for windowed variance
  ;====================================================================================================
  winsz = 30
  vals = new((/num_t/),logical)
  vals = False
    total_variance = variance(index(:,{xlon}))
    do t = winsz/2,num_t-1-winsz/2
      window_variance = variance(index(t-winsz/2:t+winsz/2,{xlon}))
      if window_variance.gt.total_variance then
        vals(t) = True
      end if
    end do

  ;====================================================================================================
  ;====================================================================================================
 do v = 0,num_v-1
  
  ofile = idir(c)+"lagreg.vert."+waveName+".hmax"+hMax+"."+var(v)+".nc"
  
  V = new((/num_t,num_lev/),float)
    V!0 = "time"
    V!1 = "lev"
    V&time = time
    V&lev  = lev

  print("============================================================================================================")
  print("")
  print("case: "+case(c)+"  var: "+var(v)+"	ofile = "+ofile)
  print("")
  print("============================================================================================================")
  

    V(:,:) = dim_avg_n( LoadERAiData4D(var(v),yr1,yr2,lat1,lat2,lon1,lon2),(/2/))


  if var(v).eq."Q" then
    V = V * 1000.
    V@units = "g/kg"
  end if
 ;==================================================================================================== 
 ; Calculate Regression Coefficients
 ;====================================================================================================
  lag = ispan(-mxlag,mxlag,1)
  RC = new((/num_lev,mxlag*2+1/),float)
    RC!0 = "lev"
    RC!1 = "lag"
    RC&lev = lev
    RC&lag = lag
    
    tvals = ind(vals)
    if all(ismissing(tvals)) then
      print("!!!!!! ERROR !!!!!!!")
    end if
    
  num_pts = dimsizes(tvals)
  CC = RC
  sigmaY = RC
  sigmaX = new((/1/),float)
  sigmaX = dim_stddev  (index(tvals,{xlon(c)}))
    
    do l = -mxlag,mxlag
      RC(:,l+mxlag) = (/ regCoef(index(tvals,{xlon}),V(lev|:,time|tvals+l)) /)
      CC(:,l+mxlag) = escorc( index(tvals,{xlon(c)}), V(lev|:,time|tvals+l))
      sigmaY(:,l+mxlag) = dim_stddev_n(V(lev|:,time|tvals+l),1)      
    end do
      delete(tvals)
 ;====================================================================================================
 ; Write to file
 ;====================================================================================================
    if isfilepresent(ofile) then
      system("rm "+ofile)
    end if
    outfile = addfile(ofile,"c")
    outfile->RegCoef = RC
    outfile->CorCoef = CC
    outfile->sigmaX = sigmaX
    outfile->sigmaY = sigmaY
    outfile->num_pts = num_pts
    outfile->lag = lag
    outfile->lev = lev
    outfile@var = var(v)
    outfile@center_lon = xlon
    outfile@lat1 = lat1
    outfile@lat2 = lat2
    outfile@yr1 = yr1
    outfile@yr2 = yr2
    outfile@mxlag = mxlag
    
 end do    
end do
;====================================================================================================
;====================================================================================================
end

