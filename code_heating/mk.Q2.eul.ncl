load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_CESM.ncl"

begin

	case = (/"raspe_eul_06","raspe_eul_07","raspe_eul_09","raspe_eul_10","raspe_eul_11"/)
	;case = (/"raspe_eul_06"/)
	
	dir  = "/maloney-scratch/whannah/CESM/data/"+case+"/raw_data/"
	odir = "/maloney-scratch/whannah/CESM/data/"+case+"/"
	
	var = "Q2"
	
	lat1 = -30.
	lat2 =  30.
	
	yr1 = 2
	yr2 = 8
	
		cpd 	= 1004.		; J / (kg K)
		cpv 	= 1850.		; J / (kg K)
		Tf	= 273.15 	; K
		g  	= 9.81		; m / s^2
		Lv  	= 2.5*10.^6.	; J / kg
		Po	= 100000.	; Pa
		Rd	= 287.		; J / (kg K)
		Rv	= 461.		; J / (kg K)
		esf	= 611.		; Pa
		eps	= 0.622		; (epsilon)
	
	num_c = dimsizes(case)
	
	num_t = (yr2-yr1+1)*365
	
	lev  = (/ 975.,925.,850.,800.,700.,600.,500.,400.,300.,200.,150.,100./)		
	ilev = (/1000.,950.,900.,825.,750.,650.,550.,450.,350.,250.,175.,125.,50./)	
	
;====================================================================================================
;====================================================================================================

    ;--------------------------------------
    ; coordinate variables
    ;--------------------------------------
      lat = LoadCESMlat(case(0),lat1,lat2)
      lon = LoadCESMlon(case(0),0.,360.)
      
      
        print("")
        print("  var : "+var)
        print("    lat = 	"+sprintf("%6.4g",min(lat))     +"	:	"+sprintf("%6.4g",max(lat)))
        print("    lon = 	"+sprintf("%6.4g",min(lon))     +"	:	"+sprintf("%6.4g",max(lon)))
        print("    lev = 	"+sprintf("%6.4g",max(lev))+"	:	"+sprintf("%6.4g",min(lev)))
        print("")
      latsz = dimsizes(lat)
      lonsz = dimsizes(lon)
      levsz = dimsizes(lev)
      global_lat   = LoadCESMlat(case(0),-90.,90.)
      global_latsz = dimsizes(global_lat)

      
;====================================================================================================
;====================================================================================================
do c = 0,num_c-1

		print("")
        	print("case: "+case(c))
        	print("")
       
        Q    = LoadCESMData4D   (case(c),"Q",yr1,yr2,lev,-90.,90.,0.,360.)
    	 
        	Q!0 = "time"
        	Q!1 = "lev"
        	Q!2 = "lat"
        	Q!3 = "lon"
        	Q&lev = lev
        	Q&lat = global_lat
        	Q&lon = lon
     
        
      ;-----------------------------------------------------------
      ; Total Tendency   
      ;-----------------------------------------------------------   
        dt = 2.*3600.*24.
        dQdt = new((/num_t,levsz,latsz,lonsz/),"float")
        dQdt(1:num_t-2,:,:,:) = ( Q(2:num_t-1,:,{lat1:lat2},:) - Q(0:num_t-3,:,{lat1:lat2},:) ) / dt
      ;-----------------------------------------------------------   
      ; Calculate horizontal gradients
      ;----------------------------------------------------------- 
        Qraw = LoadCESMData4Draw(case(c),"Q",yr1,yr2,-90.,90.,0.,360.)  
        Uraw = LoadCESMData4Draw(case(c),"U",yr1,yr2,-90.,90.,0.,360.)
        Vraw = LoadCESMData4Draw(case(c),"V",yr1,yr2,-90.,90.,0.,360.)
        PS   = LoadCESMData(case(c),"PS"  ,yr1,yr2,-999.,-90.,90.,0.,360.)
        P0   = LoadCESMhy(case(c),"P0")
        hyam = LoadCESMhy(case(c),"hyam")
        hybm = LoadCESMhy(case(c),"hybm")
        rlevsz = dimsizes(Qraw(0,:,0,0))
        nt = num_t
          rawQU_dx = new((/nt,rlevsz,global_latsz,lonsz/),float)
          rawQU_dy = new((/nt,rlevsz,global_latsz,lonsz/),float)
          rawQV_dx = new((/nt,rlevsz,global_latsz,lonsz/),float)
          rawQV_dy = new((/nt,rlevsz,global_latsz,lonsz/),float)
          ;rawQ_dx  = new((/nt,rlevsz,global_latsz,lonsz/),float)
          ;rawQ_dy  = new((/nt,rlevsz,global_latsz,lonsz/),float)
        gradsg(Qraw*Uraw,rawQU_dx,rawQU_dy)
        gradsg(Qraw*Vraw,rawQV_dx,rawQV_dy)
        ;gradsg(Qraw     ,Q_dx ,Q_dy)
        QU_dx = vinth2p(rawQU_dx,hyam,hybm,lev,PS,1,P0,1,False)
        QV_dy = vinth2p(rawQU_dx,hyam,hybm,lev,PS,1,P0,1,False)
          delete([/Qraw,Uraw,Vraw,hyam,hybm,PS,P0/])
          delete([/rawQU_dx,rawQU_dy,rawQV_dx,rawQV_dy/])
          copy_VarCoords(Q,QU_dx)
          copy_VarCoords(Q,QV_dy)
          ;copy_VarCoords(Q,Q_dx)
          ;copy_VarCoords(Q,Q_dy)
        dQUdx = QU_dx(:,:,{lat1:lat2},:)
        dQVdy = QV_dy(:,:,{lat1:lat2},:)
        ;UdQdx = U(:,:,{lat1:lat2},:)*Q_dx(:,:,{lat1:lat2},:)
        ;VdQdy = V(:,:,{lat1:lat2},:)*Q_dy(:,:,{lat1:lat2},:)
          delete([/QU_dx,QV_dy/])
          ;delete([/Q_dx,Q_dy/])
          delete([/Q/])
      ;-----------------------------------------------------------
      ; Horizontal advection/convergence
      ;-----------------------------------------------------------
        ;VdelQ = UdQdx + VdQdy
        delQV = dQUdx + dQVdy
          ;delete([/UdQdx,VdQdy/])
          delete([/dQUdx,dQVdy/])
      ;-----------------------------------------------------------
      ; Vertical advection/convergence
      ;-----------------------------------------------------------
        ;W  = LoadCESMData4D(case(c),"OMEGA",yr1,yr2,lev ,lat1,lat2,0.,360.)
        Wi = LoadCESMData4D(case(c),"OMEGA",yr1,yr2,ilev,lat1,lat2,0.,360.)
        Qi = LoadCESMData4D(case(c),"Q"    ,yr1,yr2,ilev,lat1,lat2,0.,360.)
        QWi = Qi*Wi
          delete([/Wi/])
        ;------------------------------------------------
        ;------------------------------------------------
        num_lev = levsz
        num_ilev = dimsizes(ilev)
        kt1 = 1
        kt2 = num_ilev-1
        kb1 = 0
        kb2 = num_ilev-2
        ;------------------------------------------------
        ;------------------------------------------------
        dP = conform(Qi(:,kb1:kb2,:,:),ilev(kt1:kt2) - ilev(kb1:kb2),1) *100.	
        ;------------------------------------------------
        ;------------------------------------------------
        ;dQdP  = (Qi (:,kt1:kt2,:,:) - Qi (:,kb1:kb2,:,:) ) / abs(dP)
        dQWdP = (QWi(:,kt1:kt2,:,:) - QWi(:,kb1:kb2,:,:) ) / abs(dP)
          delete([/Qi,QWi,dP/])
      ;-----------------------------------------------------------
      ;-----------------------------------------------------------
        Q2 = -Lv*( dQdt + delQV + dQWdP )
        ;Q2 = -Lv*( dQdt + VdelQ + W*dQdP )
          delete([/dQdt,delQV,dQWdP/])
          ;delete([/dQdt,VdelQ,dQdP,W/])
        
        unit_str = "J/s"

	Q2!0 = "time"
	Q2!1 = "lev"
        Q2!2 = "lat"
        Q2!3 = "lon"
        Q2&lev = lev
        Q2&lat = lat
        Q2&lon = lon
        Q2@long_name = "Apparent Moisture Sink"
        Q2@units     = unit_str
      ;-----------------------------------------------------------
      ;-----------------------------------------------------------
      ofile = odir(c)+case(c)+"."+var+"."+yr1+"-"+yr2+".nc"
        if isfilepresent(ofile) then
          system("rm "+ofile) 
        end if
        outfile = addfile(ofile,"c")
        outfile->Q2 = Q2
        
      ;print(outfile)
      print("")
      print("	ofile = "+ofile)
      print("")
        delete([/Q2/])
    ;***********************************************************************************
    ;***********************************************************************************
end do
;====================================================================================================
;====================================================================================================
end




