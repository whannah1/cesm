load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_obs.ncl"
load "$NCARG_ROOT/custom_functions_CESM.ncl"

begin

	case = (/"ERAi"/)
	
	
	odir = "/data/whannah/obs/"+case+"/yearly_data_all_lons/"
	
	var = "Q1"
	
	lat1 = -30.
	lat2 =  30.
	
	yr1 = 0
	yr2 = 9
	
		cpd 	= 1004.		; J / (kg K)
		cpv 	= 1850.		; J / (kg K)
		Tf	= 273.15 	; K
		g  	= 9.81		; m / s^2
		Lv  	= 2.5*10.^6.	; J / kg
		Po	= 100000.	; Pa
		Rd	= 287.		; J / (kg K)
		Rv	= 461.		; J / (kg K)
		esf	= 611.		; Pa
		eps	= 0.622		; (epsilon)
	
	num_c = dimsizes(case)
	
	num_t = (yr2-yr1+1)*365
	
;====================================================================================================
;====================================================================================================

    ;--------------------------------------
    ; coordinate variables
    ;--------------------------------------
      lat = LoadERAilat(lat1,lat2)
      lon = LoadERAilon(0.,360.)
      lev = LoadERAilev()*100.
      
;print(lev)
      
        print("")
        print("  case: "+case)
        print("  var : "+var)
        print("    lat = 	"+sprintf("%6.4g",min(lat))     +"	:	"+sprintf("%6.4g",max(lat)))
        print("    lon = 	"+sprintf("%6.4g",min(lon))     +"	:	"+sprintf("%6.4g",max(lon)))
        print("    lev = 	"+sprintf("%6.4g",max(lev/100.))+"	:	"+sprintf("%6.4g",min(lev/100.)))
        print("")
      latsz = dimsizes(lat)
      lonsz = dimsizes(lon)
      levsz = dimsizes(lev)
      global_lat   = LoadERAilat(-90.,90.)
      global_latsz = dimsizes(global_lat)

      
;====================================================================================================
;====================================================================================================
        T = LoadERAiData4D("T"  ,yr1,yr2,-90.,90.,0.,360.)
        Z = LoadERAiData4D("GEO",yr1,yr2,-90.,90.,0.,360.)
        
        S = T*cpd + Z
        	
        	S@long_name = "Dry Static Energy"
        	S!0 = "time"
        	S!1 = "lev"
        	S!2 = "lat"
        	S!3 = "lon"
        	S&lev = lev
        	S&lat = global_lat
        	S&lon = lon
        	
          delete([/T,Z/])
     
      ;-----------------------------------------------------------
      ; Total Tendency   
      ;-----------------------------------------------------------   
        dt = 2.*3600.*24.
        num_y = (yr2-yr1+1)
        dSdt = new((/365*num_y,levsz,latsz,lonsz/),"float")
        dSdt(1:num_y*365-2,:,:,:) = ( S(2:num_y*365-1,:,{lat1:lat2},:) - S(0:num_y*365-3,:,{lat1:lat2},:) ) / dt
        	
do y = yr1,yr2


        t1 = y*365
        t2 = y*365+364
        U = LoadERAiData4D("U"  ,y,y,-90.,90.,0.,360.)
        V = LoadERAiData4D("V"  ,y,y,-90.,90.,0.,360.)
      ;-----------------------------------------------------------   
      ; Calculate horizontal gradients
      ;----------------------------------------------------------- 
        	xvals = ispan(0,lonsz-1,1)
		Rvals = (xvals+lonsz+1)%lonsz
		Lvals = (xvals+lonsz-1)%lonsz
		yvals = ispan(0,global_latsz-1,1)
		Nvals = yvals(2:global_latsz-1)
		Cvals = yvals(1:global_latsz-2)
		Svals = yvals(0:global_latsz-3)

        SU_dx = new((/365,levsz,global_latsz-2,lonsz/),float)
        SV_dy = new((/365,levsz,global_latsz-2,lonsz/),float)
            
        SU = S(t1:t2,:,:,:)*U
        SV = S(t1:t2,:,:,:)*V

        dx = tofloat( (lon(2)-lon(0))*111000.*conform(SU_dx,cos(global_lat(Cvals)*3.14159/180.),2) )
        dy = tofloat( (lat(2)-lat(0))*111000. )
        
        SU_dx = ( SU(:,:,Cvals,Rvals) - SU(:,:,Cvals,Lvals) )/dx
        SV_dy = ( SV(:,:,Nvals,:)     - SV(:,:,Svals,:)     )/dy 	
        
			SU_dx!2     = "lat"
			SU_dx&lat   = global_lat(Cvals)
			SV_dy!2     = "lat"
			SV_dy&lat   = global_lat(Cvals)
      
        dSUdx = SU_dx(:,:,{lat1:lat2},:)
        dSVdy = SV_dy(:,:,{lat1:lat2},:)
          delete([/SU_dx,SV_dy/])
      ;-----------------------------------------------------------
      ; Horizontal advection/convergence
      ;-----------------------------------------------------------
        delSV = dSUdx + dSVdy
          delete([/dSUdx,dSVdy/])
      ;-----------------------------------------------------------
      ; Vertical advection/convergence
      ;-----------------------------------------------------------
        W  = LoadERAiData4D    ("OMEGA",y,y,lat1,lat2,0.,360.)
        Wi = LoadERAiData4Dilev("OMEGA",y,y,lat1,lat2,0.,360.)
        Zi = LoadERAiData4Dilev("GEO"  ,y,y,lat1,lat2,0.,360.)
        Ti = LoadERAiData4Dilev("T"    ,y,y,lat1,lat2,0.,360.)
        ilev = Ti&lev
        Si  = Ti*cpd + Zi
        SWi = Wi*Si
          delete([/Ti,Zi/])
        ;------------------------------------------------
        ;------------------------------------------------
        num_lev = levsz
        num_ilev = dimsizes(ilev)
        kt1 = 1
        kt2 = num_ilev-1
        kb1 = 0
        kb2 = num_ilev-2
        ;------------------------------------------------
        ;------------------------------------------------
        dP = conform(SWi(:,kb1:kb2,:,:),ilev(kt1:kt2) - ilev(kb1:kb2),1) *100.
        ;------------------------------------------------
        ;------------------------------------------------
        dSWdP = (SWi(:,kt1:kt2,:,:) - SWi(:,kb1:kb2,:,:) ) / abs(dP)
          delete([/dP,SWi,Si,Wi,W/])
      ;-----------------------------------------------------------
      ;-----------------------------------------------------------
        Q1 = dSdt(t1:t2,:,:,:) + delSV + dSWdP
        
        unit_str = "J/s"

		Q1!0 = "time"
		Q1!1 = "lev"
        Q1!2 = "lat"
        Q1!3 = "lon"
        Q1&lev = lev
        Q1&lat = lat
        Q1&lon = lon
        Q1@long_name = "Apparent Heat Source"
        Q1@units     = unit_str

      ;-----------------------------------------------------------
      ;-----------------------------------------------------------

      ofile = odir+"ERAi."+var+".200"+y+".no_leap.nc"
        if isfilepresent(ofile) then
          system("rm "+ofile) 
        end if
        outfile = addfile(ofile,"c")
        outfile->$var$ =  Q1
      
      print(outfile)
      print(ofile)
      print("")
      print(lev)
      
    ;***********************************************************************************
    ;***********************************************************************************
end do
;====================================================================================================
;====================================================================================================
end

