; This script plots the last ndays of the CESM simulation
; indicated by the "case" variable. 6 hovmoller diagrams are
; plotted for various quantities. This is mena to serve a 
; simple first look at the model output.
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin
    
    case = "AQUA_ZM_00_1.9x2.5_344"

    idir = "~/Data/Model/CESM/scratch/"+case+"/run/"

    fig_type = "png"
    fig_file = "~/Research/CESM/figs_hov/hov.v1."+case

    lat1 = -10.
    lat2 =  10.

    ndays = 180
    daily = True

;===================================================================================
; Load CESM output data
;===================================================================================
    ifiles = systemfunc("ls "+idir+case+".cam.h1.*")
    nf = dimsizes(ifiles)
    ifiles := ifiles(:nf-2)
    ifiles := ifiles(nf-2-ndays:)
    infile = addfiles(ifiles,"r")

    lon = infile[0]->lon

    PRECC = infile[:]->PRECC(:,{lat1:lat2},:)
    PRECL = infile[:]->PRECL(:,{lat1:lat2},:)
    PRECT = (PRECC+PRECL)*1e3*86400.
    PRECT@long_name = "Total Precipitation"
    PRECT@units = "mm/day"
    copy_VarCoords(PRECC,PRECT)
    V1 = PRECT

    OMEGA = infile[:]->OMEGA(:,{500},{lat1:lat2},:)
    OMEGA = (/OMEGA*1e2/)
    OMEGA@units = "hPa/s"

    Q = infile[:]->Q(:,{850},{lat1:lat2},:)
    Q = (/Q*1e3/)
    Q@units = "g/kg"

    V2 = infile[:]->FLUT (:,{lat1:lat2},:)
    ;V3 = infile[:]->FSNT(:,{lat1:lat2},:)
    V3 = infile[:]->LHFLX(:,{lat1:lat2},:)
    V4 = Q
    V5 = infile[:]->U(:,{850},{lat1:lat2},:)
    ;V6 = infile[:]->U(:,{200},{lat1:lat2},:)
    V6 = OMEGA
    
    time = infile[:]->time
    nt = dimsizes(time)
    t1 = nt-ndays*4
    t2 = nt-1

    time := time(t1:t2)

    avgdim = (/1/)
    V1 := dim_avg_n_Wrap(V1(t1:t2,:,:),avgdim)
    V2 := dim_avg_n_Wrap(V2(t1:t2,:,:),avgdim)
    V3 := dim_avg_n_Wrap(V3(t1:t2,:,:),avgdim)
    V4 := dim_avg_n_Wrap(V4(t1:t2,:,:),avgdim)
    V5 := dim_avg_n_Wrap(V5(t1:t2,:,:),avgdim)
    V6 := dim_avg_n_Wrap(V6(t1:t2,:,:),avgdim)

    if daily then 
        blksz= 4
        time := block_avg(time,blksz)
        V1 := block_avg(V1,blksz)
        V2 := block_avg(V2,blksz)
        V3 := block_avg(V3,blksz)
        V4 := block_avg(V4,blksz)
        V5 := block_avg(V5,blksz)
        V6 := block_avg(V6,blksz)
    end if


    ; Load wind vector data
    ;lev = 850
    ;u = infile[:]->U(:,{lev},:,:)
    ;v = infile[:]->U(:,{lev},:,:)

;===================================================================================
; Create plot
;===================================================================================
    wks = gsn_open_wks(fig_type,fig_file)
    plot = new(6,graphic)
        res = setres_contour()
        res@trYReverse      = True
        res@tiYAxisString   = "days"
    
    plot(0) = gsn_csm_contour(wks,V1,res)
    plot(1) = gsn_csm_contour(wks,V2,res)
    plot(2) = gsn_csm_contour(wks,V3,res)
    plot(3) = gsn_csm_contour(wks,V4,res)

        ;res@cnLevelSelectionMode = "ExplicitLevels"
        ;res@cnLevels = ispan(-12,12,1)
        symMinMaxPlt(V5,15,False,res)
    plot(4) = gsn_csm_contour(wks,V5,res)
        symMinMaxPlt(V6,15,False,res)
    plot(5) = gsn_csm_contour(wks,V6,res)

    ;------------------------------------------
    ; Add phase speed reference lines
    ;------------------------------------------
        res := setres_default()
        res@xyLineThicknessF    = 2.
        
    spd1 =  5. *86400./111e3
    spd2 = 15. *86400./111e3
    spd3 = 60. *86400./111e3

    ;stime = time - time(90) 

    do p = 0,dimsizes(plot)-1
        stime := time - time(30) 
        overlay(plot(p),gsn_csm_xy(wks,stime*spd1,time,res))
        overlay(plot(p),gsn_csm_xy(wks,stime*spd2,time,res))
        stime := time - time(90) 
        overlay(plot(p),gsn_csm_xy(wks,stime*spd1,time,res))
        overlay(plot(p),gsn_csm_xy(wks,stime*spd2,time,res))
    end do
    ;------------------------------------------
    ;------------------------------------------
        pres = setres_panel()
    gsn_panel(wks,plot,(/2,3/),pres)

    trimPNG(fig_file)

;===================================================================================
;===================================================================================
end
