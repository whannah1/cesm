load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_CESM.ncl"
begin
    
    case = "AQUA_ZM_00_1.9x2.5_"+(/"444","204","404"/)
    ;case = "AQUA_ZM_00_1.9x2.5_"+(/"444"/)

    ;mem  := (/"00","05","06"/)     ; WP meridional gradient
    ;mem  := (/"00","01","02"/)     ; SST+WP meridional shift
    ;mem  := (/"00","02","04"/)     ; WP meridional shift
    mem  := (/"00","01","06","08"/)     ;  
    case := "AQUA_ZM_"+mem+"_1.9x2.5_444"

    fig_type = "png"
    fig_file = "~/Research/CESM/figs_LCT/LCT.avg.map.v1"

    debug = True

    panel_flip = True

    sdim = 1    ; set 1 for zonal, 2 for meridional

    sub_LHF = True

;===================================================================================
;===================================================================================
    num_c = dimsizes(case)

    if debug then CESM_trunclen = 1 else CESM_trunclen = 8 end if

    opt  = True
    opt@ERAIyr1  = 2000
    opt@ERAIyr2  = opt@ERAIyr1 + CESM_trunclen
    opt@debug    = False
    opt@truncate = True
    ;opt@monthly  = False
    ;opt@use6h    = True

    opt@lat1  = -30
    opt@lat2  =  30


    wks = gsn_open_wks(fig_type,fig_file)
    plot = new(num_c,graphic)
        res = setres_contour_fill_smalltxt()
        res@vpHeightF           = 0.4
        ;res@gsnAddCyclic        = True
        ;res@mpCenterLonF        = 180
        ;res@mpMinLatF           = opt@lat1
        ;res@mpMaxLatF           = opt@lat2
        ;res@mpOutlineBoundarySets ="NoBoundaries"
        res@cnLevelSelectionMode = "ExplicitLevels"
        res@gsnLeftStringFontHeightF = 0.015


        cres = setres_contour()
        cres@gsnLeftString  = ""
        cres@gsnRightString = ""
        cres@cnLevelSelectionMode = "ExplicitLevels"
        cres@cnLevels       = ispan(-6,400,2)
        cres@cnLineLabelsOn = False

        ;lres = setres_default()
;===================================================================================
;===================================================================================
do c = 0,num_c-1
    ;------------------------------------------------
    ;------------------------------------------------
    opt@monthly  = True
    TS := dim_avg_n_Wrap( LoadCESM(case(c),"TS",opt) ,0)
    TS = TS - 300.
    ;lon := TS&lon
    ;TS&lon = where(lon.lt.0,lon+360,lon)

    opt@monthly  = False
    dQdt  = LoadCESM(case(c),"dQdtvi",opt)
    UdQdx = LoadCESM(case(c),"UdQdxvi",opt)
    VdQdy = LoadCESM(case(c),"VdQdyvi",opt)
    LHFLX = LoadCESM(case(c),"LHFLX",opt)

    

    ; if c.eq.0 then 
    ;     lat = dQdt&lat
    ;     lon = dQdt&lon
    ;     num_lat = dimsizes(lat)
    ;     num_lon = dimsizes(lon)
    ;     if sdim.eq.1 then xdim = (/num_c,num_lon/) end if
    ;     if sdim.eq.2 then xdim = (/num_c,num_lat/) end if
    ;     if sdim.eq.1 then xcoord = lon end if
    ;     if sdim.eq.2 then xcoord = lat end if
    ;     LCT1 = new(xdim,float)
    ;     LCT2 = new(xdim,float)
    ;     LHF  = new(xdim,float)
    ; end if

    if sub_LHF then
        LCT := dim_avg_n_Wrap( dQdt + UdQdx + VdQdy - LHFLX ,0)
    else
        LCT := dim_avg_n_Wrap( dQdt + UdQdx + VdQdy ,0)
    end if
    
    LHF  := dim_avg_n_Wrap( LHFLX ,0)

    
    copy_VarCoords(TS,LCT)
    copy_VarCoords(TS,LHF)
    if sub_LHF then
        LCT@long_name = "LCT-LHF"
    else
        LCT@long_name = "LCT"
    end if
    LHF@long_name = "LHF"

    TS  = flip_lon(TS)
    LCT = flip_lon(LCT)
    LHF = flip_lon(LHF)


    delete([/dQdt,UdQdx,VdQdy,LHFLX/])    
    ;------------------------------------------------
    ;------------------------------------------------
    if sub_LHF then
        res@cnLevels = ispan(-140,20,20)
    else
        res@cnLevels = ispan(-160,160,20)
    end if

    plot(c) = gsn_csm_contour(wks,LCT,res)

    overlay( plot(c) , gsn_csm_contour(wks,TS,cres) )

    ; xx = (/-1.,1./)*1e4
    ; overlay( plot(c) , gsn_csm_xy(wks,xx,xx*0,lres) )
    ;------------------------------------------------
    ;------------------------------------------------
end do
;===================================================================================
;===================================================================================
    ; LCT1 = (/ LCT1 / Lv * 86400.  /)
    ; LCT2 = (/ LCT2 / Lv * 86400.  /)
    ; LHF  = (/ LHF  / Lv * 86400.  /)

    ; plot(0) = gsn_csm_xy(wks,xcoord,LCT1,res)
    ; plot(1) = gsn_csm_xy(wks,xcoord,LCT2,res)
    ; plot(2) = gsn_csm_xy(wks,xcoord,LHF,res)

    ; xx = (/-1.,1./)*1e4
    ; overlay( plot(0) , gsn_csm_xy(wks,xx,xx*0,lres) )
;===================================================================================
;===================================================================================
        pres = setres_panel()

    if panel_flip then 
        layout = (/dimsizes(plot),1/) 
    else 
        layout = (/1,dimsizes(plot)/)
    end if
    gsn_panel(wks,plot,layout,pres)
    
    trimPNG(fig_file)
;===================================================================================
;===================================================================================
end
