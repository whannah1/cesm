; plots timeseries of various global average quantities
; v1 - plot only one run at a time
; v2 - compares several runs
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_CESM.ncl"
begin
    
    ;case  = (/"ERAi","CESM_ZM_CTL","CESM_ZM_EXP"/)
    ;case  = (/"ERAi","CESM_SP_CTL","CESM_ZM_CTL","CAM_ZM_00"/)
    ;case = (/"CAM_ZM_00","CAM_ZM_01"/)
    ; case = (/"CAM_ZM_00","CAM_ZM_01","CESM_ZM_CTL","CESM_ZM_CTL_02"/)
    case = "ERAi"

    ;ver = (/"a","b"/)
    ver = (/"e"/)

    fig_type = "png"

    lat1 =   0
    lat2 =  40
    lon1 = -10
    lon2 =  20

    months = (/7,8,9/)
;===================================================================================
;===================================================================================
    case = CESM_get_case(case)
    casename = CESM_get_casename(case)

    num_c = dimsizes(case)

    opt  = True
    opt@debug    = False
    opt@ERAIyr1  = 2000
    opt@ERAIyr2  = 2009
    opt@monthly  = True
    opt@truncate = True
    opt@lat1  = lat1
    opt@lat2  = lat2
    opt@lon1  = lon1
    opt@lon2  = lon2  
    opt@lev = -1
;===================================================================================
;===================================================================================
do n = 0,dimsizes(ver)-1

    if ver(n).eq."a" then var = (/"U","V"/) end if
    if ver(n).eq."b" then var = (/"T","Q"/) end if
    if ver(n).eq."c" then var = (/"THETA","OMEGA"/) end if
    if ver(n).eq."d" then var = (/"T","Q","OMEGA"/) end if
    if ver(n).eq."e" then var = (/"T","Z","THETA"/) end if

    num_v = dimsizes(var)

    fig_file = "~/Research/CESM/figs_climo/climo.zonal_avg.basic.v1"+ver(n)

    wks = gsn_open_wks(fig_type,fig_file)
    plot = new(num_v*num_c,graphic)
        res = setres_contour_fill()
        res@trYReverse          = True
        ;res@vpHeightF           = 0.4
        res@trYMinF             = 100
        res@trYMaxF             = 975
        res@tmYLMode            = "Explicit"
        res@tmYLValues          = ispan(50,950,100)
        res@tmYLLabels          = res@tmYLValues
        res@tmXBLabelStride     = 2

        ;res@cnFillMode  = "CellFill"
        res@cnLevelSelectionMode    = "ExplicitLevels"
        ;res@cnLevels = ispan(-30,30,1)
;=====================================================
;=====================================================
do v = 0,num_v-1
    if isatt(res,"cnLevelSelectionMode") then delete(res@cnLevelSelectionMode) end if
    do c = 0,num_c-1
        
        tV := LoadCESM(case(c),var(v),opt)
        ;----------------------------------------
        ;----------------------------------------
        mn := tV@mn
        condition := mn.eq.0
        do m = 0,dimsizes(months)-1 condition = condition.or.(mn.eq.months(m)) end do
        vals := where(condition,True,False)
        tV := tV(ind(vals),:,:,:)

        if var(v).eq."T" then tV = (/tV-273.16/) end if
        ;----------------------------------------
        ;----------------------------------------
        adim = (/0,3/)
        aV := dim_avg_n_Wrap(dim_avg_n_Wrap(tV,3),0)

        copy_VarAtts(tV,aV)

        ncn = 31
        if c.eq.0 then 
        if any(var(v).eq.(/"U","V","T","OMEGA"/)) then 
            ;if var(v).eq."T" then ncn = 51 end if
            symMinMaxPlt(aV,ncn,True,res) 
        else
            ;if var(v).eq."Q" then ncn = 51 end if
            mnmxint = nice_mnmxintvl(min(aV),max(aV),ncn,True)
            res@cnLevelSelectionMode = "ManualLevels"
            res@cnMinLevelValF       = mnmxint(0)
            res@cnMaxLevelValF       = mnmxint(1)
            res@cnLevelSpacingF      = mnmxint(2)
        end if
        end if

        res@gsnRightString      = casename(c)

        ; ip = v*num_c+c
        ip = c*num_v+v

        plot(ip) = gsn_csm_contour(wks,aV,res)

    end do
end do
;=====================================================
; Combine panels
;=====================================================
        pres = setres_panel()
    ; gsn_panel(wks,plot,(/num_v,num_c/),pres)
    gsn_panel(wks,plot,(/num_c,num_v/),pres)

    trimPNG(fig_file)

end do
;===================================================================================
;===================================================================================
end
