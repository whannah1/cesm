; plot a map and zonal mean of effective static statbility (see O'Gorman 2011)
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_CESM.ncl"
begin
    
    ;case = (/"ERAi","CESM_SP_CTL","CESM_SP_EXP","CESM_ZM_CTL","CESM_ZM_EXP"/)
    ;case = (/"CESM_SP_CTL","CESM_SP_EXP"/)
    case = "ERAi"

    fig_type = "png"
    fig_file = "~/Research/CESM/figs_climo/climo.seasonal.ess.v1"

    recalc_sym  = True     ; recalculate lambda, the eddy symmetry parameter
    recalc_ess  = False     ; recalculate ess
    
    debug       = False

    panel_flip  = False      ; True ~ col = variable    

;===================================================================================
;===================================================================================
    case = where(case.eq."ERAi",case,case+"-CPL_0.9x1.25_gx1v6_01")
    casename = CESM_get_casename(case)
    if .not.isvar("casename") then casename = case end if
    num_c = dimsizes(case)

    if debug then CESM_trunclen = 1 else CESM_trunclen = 8 end if
    opt  = True
    opt@debug    = False
    opt@ERAIyr1  = 2000
    opt@ERAIyr2  = opt@ERAIyr1 + 5 - 1 ;+ CESM_trunclen-1
    ;opt@monthly  = True
    opt@truncate = True
    opt@lev = -1

    opt@lat1  = -30
    opt@lat2  =  60
    ; opt@lon1  = lon1
    ; opt@lon2  = lon2

    wks = gsn_open_wks(fig_type,fig_file)
    plot = new(num_c,graphic)
        res = setres_contour_fill()
        ;res = setres_contour()
        res@cnFillPalette   = "ncl_default"
        ;res@cnFillPalette   = "WhiteBlueGreenYellowRed"
        res@cnLevelSelectionMode = "ExplicitLevels"
        ;res@cnLevels        = ispan(5,95,5)/1e2
        

        ; res@gsnAddCyclic    = False
        ; res@mpCenterLonF    = 0.
        ; res@mpLimitMode     = "LatLon" 
        ; ;res@mpProjection    = "Aitoff"
        ; if isatt(opt,"lat1") then res@mpMinLatF = opt@lat1 end if
        ; if isatt(opt,"lat2") then res@mpMaxLatF = opt@lat2 end if
        ; if isatt(opt,"lon1") then res@mpMinLonF = opt@lon1 end if
        ; if isatt(opt,"lon2") then res@mpMaxLonF = opt@lon2 end if
;===================================================================================
;===================================================================================
do c = 0,num_c-1
    tfile1 = "~/Data/CESM/"+case(c)+"/climo.ess.lambda.v1.nc"
    tfile2 = "~/Data/CESM/"+case(c)+"/climo.ess.v1.nc"
    print("case: "+case(c)+"    "+tfile2)
    ;*******************************************************************
    ;*******************************************************************
    if recalc_sym then 
        ;---------------------------------------
        ; Calculate lambda (symmetry parameter)
        ;---------------------------------------
        W := LoadCESM(case(c),"OMEGA",opt)

        mn := W@mn

        dims := dimsizes(W)
        dims(0) = 12
        lambda = new(dims,float)
        do m = 0,11
            vals := where(mn.eq.(m+1),True,False)

            Wp    := dim_rmvmean_n(W(ind(vals),:,:,:),0)
            Wp_up := where(Wp.gt.0.,Wp,0.)
            Wp2   := dim_avg_n( Wp^2. ,0)
            Wp2   = where(Wp2.ne.0.,Wp2,Wp2@_FillValue)

            lambda(m,:,:,:) = dim_avg_n( Wp*Wp_up ,0) / Wp2

            delete([/Wp,Wp2,Wp_up/])
        end do

        lambda!0 = "month"
        lambda!1 = "lev"
        lambda!2 = "lat"
        lambda!3 = "lon"
        lambda&month = ispan(1,12,1)
        lambda&lev = W&lev
        lambda&lat = W&lat
        lambda&lon = W&lon
        ;---------------------------------------
        ; write to temporary file
        ;---------------------------------------
        if fileexists(tfile1) then system("rm "+tfile1) end if
        outfile = addfile(tfile1,"c")
        outfile->lambda = lambda
    else
        infile = addfile(tfile1,"r")
        lambda := infile->lambda
    end if
    ;*******************************************************************
    ;*******************************************************************

    ;*******************************************************************
    ;*******************************************************************

        res@gsnLeftString   = casename(c)
        res@gsnRightString  = ""

    ;pV := dim_avg_n_Wrap( dim_avg_n_Wrap( lambda , 3 ) ,1)

        res@gsnRightStringFontHeightF = 0.025
        res@gsnLeftStringFontHeightF  = 0.025

    ;     res@cnLevels = ispan(30,50,2)/1e2
    ;     res@tiXAxisString = "Month"
    ;     res@tiYAxisString = "Latitude"
    ; plot := new(3,graphic)
    ; pV = lambda(lat|:,month|:,lon|:,lev|:)
        
    ;     res@lbLabelBarOn = False
    ;     res@gsnRightString  = "200 mb"
    ; plot(0) = gsn_csm_contour(wks,dim_avg_n_Wrap( pV(:,:,:,{200}) ,2),res)
    ;     res@lbLabelBarOn = True
    ;     res@gsnRightString  = "500 mb"
    ; plot(1) = gsn_csm_contour(wks,dim_avg_n_Wrap( pV(:,:,:,{500}) ,2),res)
    ;     res@lbLabelBarOn = False
    ;     res@gsnRightString  = "800 mb"
    ; plot(2) = gsn_csm_contour(wks,dim_avg_n_Wrap( pV(:,:,:,{800}) ,2),res)


    plot := new(2,graphic)
    pV = lambda(lev|:,lat|:,month|:,lon|:)

    pV1 = dim_avg_n_Wrap(pV,(/2,3/))
    pV2 = dim_stddev_n_Wrap(pV,(/2,3/))

        res@trYReverse = True
        res@gsnRightString  = "Mean"
    plot(0) = gsn_csm_contour(wks,pV1,res)
        res@gsnRightString  = "Std. Deviation"
    plot(1) = gsn_csm_contour(wks,pV2,res)

    ;---------------------------------------
    ;---------------------------------------
end do
;===================================================================================
;===================================================================================
        pres = setres_panel()

    layout = (/1,dimsizes(plot)/)
    ;if panel_flip then layout = (/num_c,1/) end if

    ;if (num_v*num_c).eq.4 then layout = (/2,2/) end if

    gsn_panel(wks,plot,layout,pres)

    trimPNG(fig_file)

;===================================================================================
;===================================================================================

end

