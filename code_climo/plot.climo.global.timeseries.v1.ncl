; plots timeseries of various global average quantities
; v1 - plot only one run at a time
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin
    
    case = "CESM_ZM_CTL-CPL_0.9x1.25_gx1v6_02"

    ;idir = "~/Data/Model/CESM/scratch/"+case+"/run/"
    idir = "~/Data/CESM/"+case+"/atm/"

    fig_type = "png"
    fig_file = "~/Research/CESM/figs_climo/climo.global.timeseries.v1"


;===================================================================================
;===================================================================================
    ;ifiles = systemfunc("ls "+idir+case+".cam.h1.*")
    ifiles = systemfunc("ls "+idir+case+".cam.h0.*")
    nf = dimsizes(ifiles)
    ifiles := ifiles(:nf-1)
    ;ifiles := ifiles(nf-2-12*2:)
    infile = addfiles(ifiles,"r")

    printfilevars(infile[0])

    time := infile[:]->time

    nt = dimsizes(time)
    t1 = 0
    t2 = nt-1

    time := infile[:]->time(t1:t2) 
    time  = (/time/365/)
    time@units = "years since 1900-01-01 00:00:00"
    time&time = time


    PRECC = infile[:]->PRECC(t1:t2,:,:)
    PRECL = infile[:]->PRECL(t1:t2,:,:)
    PRECT = (PRECC+PRECL)*1e3*86400.
    PRECT@long_name = "Total Precipitation"
    PRECT@units = "mm/day"
    copy_VarCoords(PRECC,PRECT)
    
    V1 = PRECT
    V2 = infile[:]->TS    (t1:t2,:,:)
    V3 = infile[:]->FSNT  (t1:t2,:,:)
    V4 = infile[:]->FSNS  (t1:t2,:,:)


    ;V1 = infile[:]->TS    (t1:t2,:,:)
    ;V2 = infile[:]->PS    (t1:t2,:,:)
    ;V3 = infile[:]->ICEFRAC    (t1:t2,:,:)
    ;V4 = infile[:]->SNOWHICE    (t1:t2,:,:)

    ; Cloud fields - not very useful - TOA radiation is better
    ;V1 = infile[:]->CLDHGH    (t1:t2,:,:)
    ;V2 = infile[:]->CLDMED    (t1:t2,:,:)
    ;V3 = infile[:]->CLDLOW    (t1:t2,:,:)
    ;;V4 = infile[:]->CLDTOT    (t1:t2,:,:)
    ;V4 = infile[:]->CLOUD    (t1:t2,:,:)
    

;===================================================================================
;===================================================================================
    
    adim = (/1,2/)
    V1 := dim_avg_n_Wrap(V1,adim)
    V2 := dim_avg_n_Wrap(V2,adim)
    V3 := dim_avg_n_Wrap(V3,adim)
    V4 := dim_avg_n_Wrap(V4,adim)

    nblk = 12
    atime := block_avg(time,nblk)
    aV1 := block_avg(V1,nblk)
    aV2 := block_avg(V2,nblk)
    aV3 := block_avg(V3,nblk)
    aV4 := block_avg(V4,nblk)


    V1&time = time
    V2&time = time
    V3&time = time
    V4&time = time

;===================================================================================
;===================================================================================
    wks = gsn_open_wks(fig_type,fig_file)
    plot = new(4,graphic)
        res = setres_xy()
        res@vpHeightF           = 0.4
        res@xyLineThicknessF    = 1.
        res@xyLineColor         = "blue"
    
    plot(0) = gsn_csm_xy(wks,time,V1,res)
    plot(1) = gsn_csm_xy(wks,time,V2,res)
    plot(2) = gsn_csm_xy(wks,time,V3,res)
    plot(3) = gsn_csm_xy(wks,time,V4,res)

        res@xyLineThicknessF    = 4.
        res@xyLineColor         = "red"

    overlay( plot(0) , gsn_csm_xy(wks,atime,aV1,res) )
    overlay( plot(1) , gsn_csm_xy(wks,atime,aV2,res) )
    overlay( plot(2) , gsn_csm_xy(wks,atime,aV3,res) )
    overlay( plot(3) , gsn_csm_xy(wks,atime,aV4,res) )

        res@xyLineThicknessF    = 1.
        res@xyLineColor         = "black"

    xx = (/0,2*max(time)/)
    yy = (/1.,1./)
    overlay( plot(0) , gsn_csm_xy(wks,xx,yy*avg(aV1),res) )
    overlay( plot(1) , gsn_csm_xy(wks,xx,yy*avg(aV2),res) )
    overlay( plot(2) , gsn_csm_xy(wks,xx,yy*avg(aV3),res) )
    overlay( plot(3) , gsn_csm_xy(wks,xx,yy*avg(aV4),res) )

    ;V1 = (/ V1 - conform(V1,V1(0,:),1) /)
    ;V2 = (/ V2 - conform(V2,V2(0,:),1) /)
    ;V3 = (/ V3 - conform(V3,V3(0,:),1) /)
    ;V4 = (/ V4 - conform(V4,V4(0,:),1) /)

    ;plot(4) = gsn_csm_xy(wks,V1,res)
    ;plot(5) = gsn_csm_xy(wks,V2,res)
    ;plot(6) = gsn_csm_xy(wks,V3,res)
    ;plot(7) = gsn_csm_xy(wks,V4,res)

    ;------------------------------------------
    ; Combine panels
    ;------------------------------------------
        pres = setres_panel()
    ;gsn_panel(wks,plot,(/1,dimsizes(plot)/),pres)
    gsn_panel(wks,plot,(/2,2/),pres)

    trimPNG(fig_file)

;===================================================================================
;===================================================================================
end
