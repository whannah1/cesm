; plots binned average profiles of budget terms
; v1 - basic version - no filtering
; v2 - filter x-axis variable - 2-10 day
; v3 - same as v1 but plots difference
; v4 - same as v2 but plots difference
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_CESM.ncl"
begin
    
    case1 = (/"CESM_SP_CTL","CESM_ZM_CTL"/)
    case2 = (/"CESM_SP_EXP","CESM_ZM_EXP"/)
    ; case1 = "CESM_SP_CTL"
    ; case2 = "CESM_SP_EXP"
    

    xvar = "PRECT"
    
    var = (/"Cpk"/)
    ; var = (/"WbdKpdp","WpdKpdp"/)
    ; var = (/"UbdKpdx", "UpdKpdx", "VbdKpdy", "VpdKpdy"/)
    ; var = (/"Ap_Qp","Ap_Bc","Ap_R"/)

    months = (/7,8,9/)
    season  = "JAS"
    
    fig_type = "png"
    fig_file = "~/Research/CESM/figs_climo/climo.budget.bin.profile.v4"
;===================================================================================
;===================================================================================
    lat1 =   0
    lat2 =  20
    lon1 = -20
    lon2 =  20

    case1 = CESM_get_case(case1)
    case2 = CESM_get_case(case2)
    casename1 = CESM_get_casename(case1)
    casename2 = CESM_get_casename(case2)
    num_c = dimsizes(case1)
    num_v = dimsizes(var)

    CESM_trunclen = 4

    opt  = True
    opt@debug = False
    opt@ERAIyr1 = 2000
    opt@ERAIyr2 = opt@ERAIyr1 + CESM_trunclen
    ;opt@useraw= True
    opt@use6h = True
    opt@monthly  = False
    opt@truncate = True
    opt@lat1  = lat1-2
    opt@lat2  = lat2+2
    opt@lon1  = lon1
    opt@lon2  = lon2  
    opt@lev = -1

    fca = 1./(10.*4)   ; longer period
    fcb = 1./( 2.*4)   ; shorter period
    opt = False       
    nwgt  = 15*4+1
    sigma = 1.0
    wgtBP = filwgts_lanczos (nwgt,2,fca,fcb,sigma)
;===================================================================================
;===================================================================================
    wks = gsn_open_wks(fig_type,fig_file)
    ;plot = new(num_v*num_c,graphic)
    plot = new(num_c*num_v,graphic)
        res = setres_contour_fill()
        res@trYReverse          = True
        res@vpHeightF           = 0.4
        res@gsnYAxisIrregular2Linear = True
        res@gsnLeftStringFontHeightF   = 0.025
        res@gsnCenterStringFontHeightF = 0.025
        res@gsnRightStringFontHeightF   = 0.025
        res@gsnLeftString       = ""
        res@gsnRightString      = ""
        res@tiYAxisString       = "Pressure [hPa]"
        res@lbTitleString       = "";"[PVU]"
        res@lbTitleFontHeightF   = 0.02
        res@lbLabelFontHeightF   = 0.02
        res@tmXBLabelFontHeightF = 0.02
        res@tmYLLabelFontHeightF = 0.02
        res@tmYLMode            = "Explicit"
        res@tmYLValues          = ispan(50,950,100)
        res@tmYLLabels          = res@tmYLValues
        res@trYMinF             = 100
        res@trYMaxF             = 975
        res@cnLevelSelectionMode    = "ExplicitLevels"
        
        lres = setres_default()
;===================================================================================
;===================================================================================
do c = 0,num_c-1    
    printline()
    print("case1 : "+casename1(c))
    print("case2 : "+casename2(c))
    do v = 0,num_v-1
        print("    "+var(v))
        ;=====================================================
        ;=====================================================
        iX1 := LoadCESM(case1(c),xvar,opt)
        iX2 := LoadCESM(case2(c),xvar,opt)
        ; if xvar.eq."PRECT".and.all(case(c).ne.(/"ERAi","TRMM"/)) then
        ;     iX = (/iX*86400.*1e3/)
        ; end if
        lat := iX1&lat
        lon := iX1&lon
        mn  := iX1@mn

        tX1  := wgt_runave_n_Wrap(iX1,wgtBP,0,0)
        tX2  := wgt_runave_n_Wrap(iX2,wgtBP,0,0)
        ;=====================================================
        ;=====================================================
        condition := mn.eq.0
        do m = 0,dimsizes(months)-1 condition = condition.or.(mn.eq.months(m)) end do
        vals := where(condition,ispan(0,dimsizes(mn)-1,1),-1)
        if .not.all(ismissing(vals)) then
            vals:= vals(ind(vals.ge.0))
            X1  := tX1 (vals,:,:)
            X2  := tX2 (vals,:,:)
        end if
        printMAM(X1)
        printMAM(X2)
        ;=====================================================
        ;=====================================================
        tvar = var(v)
        if var(v).eq."PPVQ" then tvar = "PPVQ1" end if
        if var(v).eq."LPVT" then tvar = "PVQ" end if
        if var(v).eq."LQT"  then tvar = "dQdt" end if
        if var(v).eq."DIV"  then tvar = "U" end if
        if var(v).eq."EKE_ADV"  then tvar = "UbdKpdx" end if
        if var(v).eq."EKE_SRC"  then tvar = "Cpk" end if
        if var(v).eq."EKE_RES"  then tvar = "dKpdt" end if

        iY1 := LoadCESM(case1(c),tvar,opt)
        iY2 := LoadCESM(case2(c),tvar,opt)
        Y1  := iY1(vals,:,:,:)
        Y2  := iY2(vals,:,:,:)

        lev = Y1&lev
        ;------------------------------------------------
        ;------------------------------------------------
        if any(var(v).eq.(/"PVQ","WdPVdp","Q1"/))       then X = (/ X * 86400. /) end if
        if any(var(v).eq.(/"Bt","Cpk","GFC","Bt2","WbdKpdp","WpdKpdp","UbdKpdx","UpdKpdx","VbdKpdy","VpdKpdy"/)) then 
            Y1 = (/ Y1 * -1. * 86400. /) 
            Y2 = (/ Y2 * -1. * 86400. /) 
            Y1@units = "m2 s-2 day-1"
            Y2@units = "m2 s-2 day-1"
        end if
        ;------------------------------------------------
        ;------------------------------------------------
        bins = True
        bins@verbose = False
        bins@bin_min = -20
        bins@bin_max =  20
        bins@bin_spc =  2

        num_lev = dimsizes(lev)
        num_bin = toint( ( bins@bin_max - bins@bin_min + bins@bin_spc )/bins@bin_spc )
        xbin    = fspan(bins@bin_min,bins@bin_max,num_bin)
        
        binval1  := new((/num_lev,num_bin/),float)
        binval2  := new((/num_lev,num_bin/),float)
        binpct1  := new((/num_lev,num_bin/),float)
        binpct2  := new((/num_lev,num_bin/),float)

        tX1 := block_avg(X1,4)
        tX2 := block_avg(X2,4)
        tY1 := block_avg(Y1,4)
        tY2 := block_avg(Y2,4)
        X1  := tX1
        X2  := tX2
        Y1  := tY1
        Y2  := tY2

        do k = 0,num_lev-1  

            tmp = bin_YbyX(Y1(:,k,:,:),X1,bins)
            binval1(k,:) = tmp
            binpct1(k,:) = tmp@pct
            delete([/tmp/])

            tmp = bin_YbyX(Y2(:,k,:,:),X2,bins)
            binval2(k,:) = tmp
            binpct2(k,:) = tmp@pct
            delete([/tmp/])
        end do

        binval1!0 = "lev"
        binval1!1 = "bin"
        binval1&lev = lev
        binval1&bin = xbin

        ; binval1@lon_name = "Binned Average 1"
        ; binval2@lon_name = "Binned Average 2"
        ; printMAM(binval1)
        ; printMAM(binval2)

        binval_diff = binval2 - binval1
        copy_VarCoords(binval1,binval_diff)
        binval_diff@lon_name = "Binned Average Difference"
        printMAM(binval_diff)
        ;------------------------------------------------
        ;------------------------------------------------
        if var(v).eq."Cpk"   then res@cnLevels := ispan(-35,35,5) end if
        ;------------------------------------------------
        ;------------------------------------------------
            ; res@cnFillPalette = "ncl_default"
            res@cnFillPalette = "BlueWhiteOrangeRed"

            res@gsnLeftString       = casename2(c)+" - "+casename1(c)
            res@gsnRightString      = var(v)
            ; res@gsnCenterString     = lon1+":"+lon2 + "   ("+season+")"

        ip = v*num_c+c
        plot(ip) = gsn_csm_contour(wks,binval_diff,res)

        ;------------------------------------------------
        ;------------------------------------------------
            lres1 = lres
            lres2 = lres
            lres1@tmYROn = False
            lres2@tiYAxisString = "% of observations"

        pct := (/binpct1(0,:),binpct2(0,:)/)

        overlay(plot(ip),gsn_csm_xy2(wks,xbin,pct*0,pct,lres1,lres2))
        ;------------------------------------------------
        ;------------------------------------------------
    end do
end do
printline()
;===================================================================================
; Combine panels
;===================================================================================
        pres = setres_panel()
    gsn_panel(wks,plot,(/num_v,num_c/),pres)

    trimPNG(fig_file)

;===================================================================================
;===================================================================================

end
