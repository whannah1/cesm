load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_CESM.ncl"
begin

    case = "ERAi"

    fig_type = "png"
    fig_file = "~/Research/CESM/PKE.test.v1"

;===================================================================================
;===================================================================================
    opt  = True
    opt@debug    = False
    opt@ERAIyr1  = 2000
    opt@ERAIyr2  = 2000
    opt@lat1  =  5.
    opt@lat2  = 15. 
    opt@lon1  = -5.
    opt@lon2  =  0.
    opt@lev   = 600

    fca = 1./(10.*4)   ; longer period
    fcb = 1./( 2.*4)   ; shorter period
    opt = False       

    nwgt  = 30*4+1
    sigma = 1.0
    wgtbp = filwgts_lanczos (nwgt,2,fca,fcb,sigma)
    wgthp = filwgts_lanczos (nwgt,1,fca,fcb,sigma)
    wgtlp = filwgts_lanczos (nwgt,0,fca,fcb,sigma)

;===================================================================================
;===================================================================================
    iu = LoadCESM(case,"U",opt)
    iv = LoadCESM(case,"V",opt)

    u = dim_avg_n(iu,(/1,2/))
    v = dim_avg_n(iv,(/1,2/))

    if False
        ub1 = wgt_runave_n_Wrap(u,wgtlp,0,0)
        ub2 = wgt_runave_n_Wrap(ub1,wgtlp,0,0)
        ubr = ub1 - ub2
        printline()
        printMAM(ub1)
        printMAM(ub2)
        printMAM(ubr)
        printline()
        exit
    end if

    ub = wgt_runave_n_Wrap(u,wgtlp,0,0) ;- avg(u)
    vb = wgt_runave_n_Wrap(v,wgtlp,0,0) ;- avg(u)

    up = wgt_runave_n_Wrap(u,wgthp,0,0)
    vp = wgt_runave_n_Wrap(v,wgthp,0,0)

    ; Calculate KE components
    TKE = ( u ^2 + v ^2 )/2.
    BKE = ( ub^2 + vb^2 )/2.
    PKE = ( up^2 + vp^2 )/2. + ub*up + vb*vp

    ; Calculate Residual KE
    RKE = TKE - ( BKE + PKE )

    DKE = PKE - BKE
;===================================================================================
;===================================================================================
    TKE@long_name = "Total KE"
    BKE@long_name = "Background KE"
    PKE@long_name = "Perturbation KE"
    RKE@long_name = "Residual KE"

    printline()
    printMAM(TKE)
    printMAM(BKE)
    printMAM(PKE)
    printMAM(RKE)
    printline()

;===================================================================================
;===================================================================================
    wks = gsn_open_wks(fig_type,fig_file)
    plot = new(2,graphic)
        res = setres_xy()
        res@tiXAxisString       = "[days]"
        res@tiYAxisString       = "[m~S~2~N~ s~S~-2~N~]"
        res@xyLineThicknessF    = 2.
        res@xyDashPattern       = 0
        res@xyLineColors        = (/"black","red"/)


    plot(0) = gsn_csm_y(wks,(/BKE,PKE/),res)


        res@xyLineColors        = (/"black","green"/)
    plot(1) = gsn_csm_y(wks,(/DKE*0.,DKE/),res)

    gsn_panel(wks,plot,(/1,dimsizes(plot)/),setres_panel_labeled())

    trimPNG(fig_file)

;===================================================================================
;===================================================================================

end