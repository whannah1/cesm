load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"

begin

	case = (/"eapsi_00","eapsi_01"/)

	idir = "~/CESM/data/"+case+"/raw/"
	
	odir = "~/CESM/data/"+case+"/"
	
	ifile_stub = "ras_ctrl.cam2.h0."
	
	var = (/"T"/)

	
	f_num_dy = 10	; number of days in each file
	f_num_sd = 24 	; number of samples per day in each file
	
	lat1 = 63	; 30 S
	;lat1 = 69	; 25 S
	;lat1 = 74	; 20 S
	;lat1 = 79	; 15.5 S
	
	;lat2 = 112	; 15.5 N
	;lat2 = 117	; 20 N
	;lat2 = 122	; 25 N
	lat2 = 128	; 30 N

;======================================================================================
;======================================================================================	

    fsz = f_num_dy * f_num_sd
    
    num_vars = dimsizes(var)
    
    files = systemfunc("ls "+idir+ifile_stub+"*")
    num_files = dimsizes(files)
  
    print("files:")
    print("  "+files)

    ; Make month and day arrays for file names
    mn = new(365,integer)
    dy = new(365,integer)
    days_per_month = (/31,28,31,30,31,30,31,31,30,31,30,31/)
    mn(0:30) = 1
    dy(0:30) = ispan(1,days_per_month(0),1)
    do m = 1,11
      t1 	= sum(days_per_month(0:m-1))
      t2 	= sum(days_per_month(0:m  ))-1
      mn(t1:t2) = m+1
      dy(t1:t2) = ispan(1,days_per_month(m),1)
    end do
    delete(m)

  
  
  do v = 0,num_vars-1 
    print("")
    print("  Creating file for variable: "+var(v))
  do f = 0,num_files-1
    ;--------------------------------------
    ; build input filename
    ;--------------------------------------
    
print("")
print("  f = "+f)
print("  y = "+floor(f*(fsz/f_num_sd)/365))
print("  m = "+  (mn(f*(fsz/f_num_sd)%365)))
print("  d = "+  (dy(f*(fsz/f_num_sd)%365)))

    
    y = sprinti("%4.4i", tointeger(floor(f*(fsz/f_num_sd)/365))+1 )
    m = sprinti("%2.2i", mn(f*(fsz/f_num_sd)%365)    )
    d = sprinti("%2.2i", dy(f*(fsz/f_num_sd)%365)    )

;======================================================================================
; Combine data
;======================================================================================    

if True then

    ifile = idir+ifile_stub+y+"-"+m+"-"+d+"-85200.nc"
    infile = addfile(ifile,"r")
    ;--------------------------------------------------------------------------------
    ; load data from file
    ;--------------------------------------------------------------------------------
    if f.eq.0 then
       ;--------------------------------------
       ; load coordinate variables
       ;--------------------------------------
       dims = dimsizes(dimsizes(infile->$var(v)$))     
       lat = infile->lat(lat1:lat2)
       lon = infile->lon
         print("")
         print("    lat = "+infile->lat(lat1)+"	:	"+infile->lat(lat2))
         print("    lon = "+min(lon)+"	:	"+max(lon))
       if dims.gt.3 then
         lev = infile->lev
	 print("    lev = "+max(lev)+"	:	"+min(lev))
       end if
       ;--------------------------------------
       ; Create variable to hold data
       ;--------------------------------------
         print("")
       if v.eq.0 then
         if dims.gt.3 then
           V = new((/num_files*fsz,dimsizes(lev),dimsizes(lat),dimsizes(lon)/),float)
	   print("    allocating memory "+(num_files*fsz)+" x "+dimsizes(lev)+" x "+dimsizes(lat)+" x "+dimsizes(lon))
         else
           V = new((/num_files*fsz,dimsizes(lat),dimsizes(lon)/),float)
	   print("    allocating memory "+(num_files*fsz)+" x "+dimsizes(lat)+" x "+dimsizes(lon))
         end if
	 print("")
         print("    combining monthly files...")
       end if
       ;--------------------------------------
       ; load first file
       ;--------------------------------------  
       if dims.eq.3 then
         V(0:fsz-1,:,:) = infile->$var(v)$(:,lat1:lat2,:)
       end if
       if dims.eq.4 then
	 V(0:fsz-1,:,:,:) = infile->$var(v)$(:,:,lat1:lat2,:)
       end if
       print("")
       print("      "+y+"-"+m+"-"+d)
    else 
       print("      "+y+"-"+m+"-"+d)
       ;--------------------------------------
       ; append data from other files
       ;--------------------------------------
       if dims.eq.3 then
         buff = infile->$var(v)$(:,lat1:lat2,:)
         V(f*fsz:f*fsz+fsz-1,:,:) = buff
       end if
       if dims.eq.4 then
         buff = infile->$var(v)$(:,:,lat1:lat2,:)
         V(f*fsz:f*fsz+fsz-1,:,:,:) = buff
       end if
       delete(buff)	
    end if
    ;--------------------------------------------------------------------------------
    ;--------------------------------------------------------------------------------
end if
    
  end do	; f = 0,num_files-1
  
  
  
  if dims.eq.4 then
    V!0 = "time"
    V!1 = "lev"
    V!2 = "lat"
    V!3 = "lon"
    V&lev = lev
  end if 
  if dims.eq.3 then
    V!0 = "time"
    V!1 = "lat"
    V!2 = "lon"
  end if
  
    V&lat = lat
    V&lon = lon

;======================================================================================
; Write Output File
;======================================================================================    
    
    ofile = odir + var(v)+".daily.nc"
    if isfilepresent(ofile) then
      system("rm "+ofile)
    end if
    outfile = addfile(ofile,"c")
    if dims.eq.3 then
      outfile->$var(v)$ = V(:,:,:)
    end if
    if dims.eq.4 then
      outfile->$var(v)$ = V(:,:,:,:)
    end if
    
      delete(V)

  end do	; v = 0,num_vars-1 
    
end


;======================================================================================
;======================================================================================


; lat
;(63)    -30.6282722513089
;(64)    -29.68586387434555
;(65)    -28.7434554973822
;(66)    -27.80104712041885
;(67)    -26.8586387434555
;(68)    -25.91623036649215
;(69)    -24.9738219895288
;(70)    -24.03141361256544
;(71)    -23.08900523560209
;(72)    -22.14659685863874
;(73)    -21.20418848167539
;(74)    -20.26178010471204
;(75)    -19.31937172774869
;(76)    -18.37696335078534
;(77)    -17.43455497382199
;(78)    -16.49214659685864
;(79)    -15.54973821989529
;(80)    -14.60732984293194
;(81)    -13.66492146596859
;(82)    -12.72251308900524
;(83)    -11.78010471204188
;(84)    -10.83769633507853
;(85)    -9.895287958115183
;(86)    -8.952879581151834
;(87)    -8.010471204188482
;(88)    -7.068062827225131
;(89)    -6.125654450261781
;(90)    -5.18324607329843
;(91)    -4.240837696335079
;(92)    -3.298429319371728
;(93)    -2.356020942408378
;(94)    -1.413612565445027
;(95)    -0.4712041884816759
;(96)    0.4712041884816749
;(97)    1.413612565445026
;(98)    2.356020942408376
;(99)    3.298429319371727
;(100)   4.240837696335078
;(101)   5.183246073298429
;(102)   6.12565445026178
;(103)   7.06806282722513
;(104)   8.01047120418848
;(105)   8.952879581151832
;(106)   9.895287958115183
;(107)   10.83769633507853
;(108)   11.78010471204188
;(109)   12.72251308900523
;(110)   13.66492146596859
;(111)   14.60732984293194
;(112)   15.54973821989529
;(113)   16.49214659685864
;(114)   17.43455497382199
;(115)   18.37696335078534
;(116)   19.31937172774869
;(117)   20.26178010471204
;(118)   21.20418848167539
;(119)   22.14659685863874
;(120)   23.08900523560209
;(121)   24.03141361256544
;(122)   24.97382198952879
;(123)   25.91623036649215
;(124)   26.8586387434555
;(125)   27.80104712041885
;(126)   28.7434554973822
;(127)   29.68586387434555
;(128)   30.6282722513089

