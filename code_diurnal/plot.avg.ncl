load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"


begin

	case = (/"eapsi_00","eapsi_01"/)

	ivar = "U"

	lev = 850. 

	num_c = dimsizes(case)
  	num_v = dimsizes(ivar)

	dir = "/Users/whannah/CESM/data/"+case+"/"

	pi = 3.14159
	
	num_h = 24
	
	fig_type = "x11"
	fig_file = "avg."+ivar
	
	calc_avg = True
	incl_obs = True
	
	;lat1 = 10
	;lat2 = 80
	;lon1 = 190
	;lon2 = 320
	
	m1 = 6
	d1 = 1
	
	m2 = 8
	d2 = 30
	
	day_beg = day_of_year(0,m1,d1)-2
	day_end = day_of_year(0,m2,d2)-1
	
	if isStrSubset(ivar,"PREC") then
	  tfile = dir+ivar    +".daily."+case+".nc"
	else
	  tfile = dir+ivar+lev+".daily."+case+".nc"
	end if
	
;=========================================================================================================
;=========================================================================================================

do c = 0,num_c-1
  
  files = systemfunc("ls "+dir(c)+"raw_data/"+case(c)+".cam2.h1.*")
  print(files)
  ifile = files(day_beg:day_end) 
  num_f = dimsizes(ifile)
  delete(files)  
  if c.eq.0 then
    infile= addfile(ifile(0),"r")
    latsz = dimsizes(infile->lat)
    lonsz = dimsizes(infile->lon)
    aV = new((/num_c,latsz,lonsz/),float)
    aV!0 = "case"
    aV!1 = "lat"
    aV!2 = "lon"
    aV&lat = infile->lat
    aV&lon = infile->lon
  end if
  
  if calc_avg then
  ;do v = 0,num_v-1
  v = 0
  
  print("")
  print("  case: "+case(c)+"  var: "+ivar)
  ;=========================================================================================================
  ; Load data
  ;=========================================================================================================
  infile = addfile(ifile(0),"r")
  hya = infile->hyam
  hyb = infile->hybm
  if ivar.eq."PRECT" then
    sz = dimsizes(infile->PRECC)
  else
    sz = dimsizes(infile->$ivar$)
  end if
  num_d = dimsizes(sz)
  if num_d.eq.3 then
    V = new((/num_f,sz(1),sz(2)/),float)
  end if
  if num_d.eq.4 then
    V = new((/num_f,sz(2),sz(3)/),float)
  end if
  V!0 = "time"
  V!1 = "lat"
  V!2 = "lon"
  V&lat = infile->lat
  V&lon = infile->lon
  do f = 0,num_f-1
    print("  "+ifile(f))
    infile = addfile(ifile(f),"r")
    if ivar.eq."PRECT" then
      V(f,:,:) = dim_avg_n( infile->PRECC + infile->PRECL ,0)
      V@long_name = "Total Precipitation"
    else
      if num_d.eq.3 then
        V(f,:,:) = dim_avg_n(infile->$ivar$,0)
        V@long_name = infile->$ivar$@long_name
      end if
      if num_d.eq.4 then
        V(f,:,:) = dim_avg_n(vinth2p(infile->$ivar$,hya,hyb,lev,infile->PS,1,1000.,1,False),0)
        V@long_name = ivar+lev
      end if
    end if
  end do
  
  ;=========================================================================================================
  ; Caclulate average field
  ;=========================================================================================================
  
  aV(c,:,:) = dim_avg_n(V,0)
  
  if isfilepresent(tfile(c)) then
    system("rm "+tfile(c))
  end if
  outfile = addfile(tfile(c),"c")
  outfile->$ivar$ = V
  delete(V)
  ;end do
  delete(ifile)
  else
    ;do v = 0,num_v-1
    v = 0
      infile = addfile(tfile(c),"r")
      aV(c,:,:) = dim_avg_n(infile->$ivar$,0)
    ;end do
  end if
end do  

  if calc_avg then
    exit
  end if  

	conv = 1.
	units = ""
	if ivar.eq."PRECT"
	  conv = 60.*60.*24.*1000.
	  units = "mm/day"
	end if
	if ivar.eq."PRECC"
	  conv = 60.*60.*24.*1000.
	  units = "mm/day"
	end if
	if ivar.eq."PRECL"
	  conv = 60.*60.*24.*1000.
	  units = "mm/day"
	end if
	if ivar.eq."U"
	  conv = 1.
	  units = "m/s"
	end if
	if ivar.eq."V"
	  conv = 1.
	  units = "m/s"
	end if
	
	aV = aV * conv
	aV@units = units
;=========================================================================================================
; Add obs plot
;=========================================================================================================
 if incl_obs then
  infile = addfile("/maloney-scratch/whannah/obs/GPCP/GPCP_precip.19972007.nc","r")
  dpy = day_end - day_beg + 1
  yrs = 11
  days = new(yrs*dpy,integer)
  do y = 0,yrs-1
   days(y*dpy:(y+1)*dpy-1) = ispan(day_beg,day_end,1)+365*y
  end do
  temp = infile->precip(days,:,:)
  temp@_FillValue = -99999
  oV = dim_avg_n(temp,0)
  olat = infile->lat
  olon = infile->lon
  olat@units = "degrees north"
  olon@units = "degrees east"
  oV!0 = "lat"
  oV!1 = "lon"
  oV&lat = olat
  oV&lon = olon
  
 end if
;=========================================================================================================
; Create plot
;=========================================================================================================    

  if num_d.eq.4 then
    fig_file = fig_file+"."+lev
  end if

  wks = gsn_open_wks(fig_type,fig_file)
  gsn_define_colormap(wks,"WhiteBlueGreenYellowRed")
  if incl_obs then
    plot = new(num_c+1,graphic)
  else
    plot = new(num_c,graphic)
  end if
  
	res = True
	res@gsnDraw = False
	res@gsnFrame = False
	res@gsnAddCyclic = False
	res@mpCenterLonF = 180
	res@cnLinesOn = False
	res@cnFillOn = True
	res@gsnSpreadColors = True
	res@cnLevelSelectionMode = "ManualLevels"
	res@cnLevelSpacingF = .4
	res@cnMinLevelValF = 1.
	res@cnMaxLevelValF = 8.
	;res@cnFillMode = "RasterFill"
	res@mpLimitMode = "LatLon"
	res@gsnRightString  = "JJA"
	res@gsnCenterString = ivar
	res@lbLabelAngleF = -45.
	res@mpMinLatF = 15
	res@mpMaxLatF = 70
	res@mpMinLonF = 200
	res@mpMaxLonF = 320

  do c = 0,num_c-1
    res@gsnLeftString   = case(c)
    plot(c) = gsn_csm_contour_map(wks,aV(c,:,:),res)
  end do
  
  if incl_obs then
    res@gsnLeftString   = "GPCP"
    res@gsnCenterString = "Total Precip"
    plot(c) = gsn_csm_contour_map(wks,oV(:,:),res)
  end if
  
  gsn_panel(wks,plot,(/dimsizes(plot),1/),False)    
    
    
end  

