; calculate perturbation potential vorticity (PV) and write to file
; separate file for each year (see also code_data/mk.var.fv.daily.v4.ncl)
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_CESM.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin
    
    if .not.isvar("case") then 
        ; case  = "CESM_SP_CTL-CPL_0.9x1.25_gx1v6_01"
        case  = "CAM_ZM_CTL_0.9x1.25_00"
        yr1 = 00
        yr2 = 00
    end if
    
    var = "PPV"
    cvar = "PPV"+(/1,2,3/)
    
    debug     = True

    overwrite = True

;==================================================================================
;==================================================================================
    odir = "~/Data/CESM/"+case+"/data/"

    num_c = dimsizes(case)
    num_v = dimsizes(var)

    if debug then overwrite = False end if

    opt  = True
    ; opt@debug  = debug
    opt@useraw = True
    opt@lat1   = -30
    opt@lat2   =  40
    opt@lon1   = -60
    opt@lon2   =  60
    opt@lev    = -1

if debug then 
    ;opt@monthly = True
    opt@lat1   = -30
    opt@lat2   =   0
    opt@lon1   =   0
    opt@lon2   =  30
end if    

    fca = 1./(10.*4)   ; longer period
    fcb = 1./( 2.*4)   ; shorter period
    opt = False       
    nwgt  = 15*4+1
    sigma = 1.0
    wgt   = filwgts_lanczos (nwgt,2,fca,fcb,sigma)
;==================================================================================
;==================================================================================
do c = 0,num_c-1
    printline()
    print("case: "+case(c))
    if .not.fileexists(odir(c)) then system("mkdir "+odir(c)) end if
    year = CESM_get_year(case)
    ;------------------------------------------------------------------
    ;------------------------------------------------------------------
    ;do y = year(0),year(1)
    do y = yr1,yr2
        opt@year = y
        syear = sprinti( "%0.4i" , toint( year(0)+y ) )
        do v = 0,num_v-1 
            ;--------------------------------------
            ;--------------------------------------
            ;ofile = odir(c) + case(c) +"."+ var(v) +"."+y+".nc"
            ofile = odir(c) + case(c) +"."+ var(v) +"."+syear+".nc"
            if fileexists(ofile) then
                if .not.overwrite then continue end if
                if      overwrite then system("rm "+ofile) end if
            end if
            print("    var: "+strpad(var(v),10)+"      "+ofile)
            ;--------------------------------------
            ; Do Calculation
            ;--------------------------------------
            ; if .not.debug then 
                U  := LoadCESM(case(c),"U",opt)
                V  := LoadCESM(case(c),"V",opt)
            ; end if
            T  := LoadCESM(case(c),"T",opt)
            TS := LoadCESM(case(c),"TS",opt)
            PS := LoadCESM(case(c),"PS",opt)
    
            lat := T&lat
            lon := T&lon
            
            Po  := 1e5
            P   := tofloat( conform(T,T&lev*1e2,1) )
            TH   = ( T *(Po/P )^(Rd/cpd) )
            THS := ( TS*(Po/PS)^(Rd/cpd) )


            copy_VarCoords(T,TH)
            delete(TH&lev)
            TH&lev = T&lev*1e2
            ;TH&lev@units = "Pa"
            ;dTHdp   := calc_ddp(TH)
            dTHdp   := calc_ddp2(TH,THS,PS)
            OMEGA   := 7.292*10.^-5.
            RelVort := uv2vr_cfd(U,V,lat,lon,0)
            AbsVort := conform(RelVort,2.*OMEGA*sin(lat*pi/180.),(/2/))
            Z = tofloat(AbsVort+RelVort)

            PV := -g*Z*dTHdp *10.^6.

            PPV := wgt_runave_n_Wrap(PV  ,wgt,0,0)
            
            PPV@long_name  = "Potential Vorticity "
            PPV@units = "PVU"
            copy_VarCoords(U,PPV)

; printline()
; print(dim_collapse(TH,1))
; printline()
; print(dim_collapse(Z,1))
; printline()
; print(dim_collapse(PV,1))
; printline()
; print(dim_collapse(PPV,1))
; printline()

X  := dim_avg_n_Wrap(dim_avg_n_Wrap(PPV,3),0)
printVarSummary(X)
qplot_raster(X)
exit


            delete([/U,V,T,TS,PS,THS/])
            
            ;--------------------------------------
            ;--------------------------------------
            if debug then
                printline()
                print("DEBUG MODE - Exiting before write...")
            end if
            ;--------------------------------------
            ; Write to output files
            ;--------------------------------------
            outfile = addfile(ofile,"c")
            outfile->$var(v)$ = PPV
            outfile@info = "2-10 day Perturbation Potential Vorticity = -g ( AbsVort + RelVort ) d(THETA)/dp x 10^6"
            ;--------------------------------------
            ; Calculate PPV components
            ;--------------------------------------
            dTHdp_p := wgt_runave_n_Wrap(dTHdp,wgt,0,0)
            Z_p     := wgt_runave_n_Wrap(Z    ,wgt,0,0)

            dTHdp_b := dTHdp - dTHdp_p
            Z_b     := Z     - Z_p

            num_cvar = dimsizes(cvar)
            do cv = 0,num_cvar-1

                if cv.eq.0 then comp := -g * Z_p * dTHdp_b *10.^6. end if
                if cv.eq.1 then comp := -g * Z_b * dTHdp_p *10.^6. end if
                if cv.eq.2 then 
                    comp := -g * Z_p * dTHdp_p *10.^6.
                    comp = wgt_runave_n_Wrap(comp,wgt,0,0)
                end if
                copy_VarCoords(PPV,comp)

                cfile = odir(c) + case(c) +"."+cvar(cv)+"."+syear+".nc"
                if fileexists(cfile) then
                    if .not.overwrite then continue end if
                    if      overwrite then system("rm "+cfile) end if
                end if
                
                outfile = addfile(cfile,"c")
                outfile->$cvar(cv)$ = comp

                if cv.eq.0 then outfile@info = "2-10 day Perturbation Potential Vorticity  component 1 = -g Z' dpT_bar x 10^6" end if
                if cv.eq.1 then outfile@info = "2-10 day Perturbation Potential Vorticity  component 2 = -g Z_bar dpT' x 10^6" end if
                if cv.eq.2 then outfile@info = "2-10 day Perturbation Potential Vorticity  component 3 = -g ( Z' dpT' )' x 10^6" end if

                print("        "+cfile)

            end do
            ;--------------------------------------
            ;--------------------------------------
        end do
    end do
    ;------------------------------------------------------------------
    ;------------------------------------------------------------------
end do
;==================================================================================
;==================================================================================
printline()
end
