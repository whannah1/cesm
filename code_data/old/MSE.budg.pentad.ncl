load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"


begin

  vrun = (/"cam31_ras_7/","cam31_ras_6/","cam31_ras_3/","cam31_ras_4/"/)
  ;mu   = (/0.00,0.60,0.20,0.20/)
  ;evap = (/0.30,0.30,0.05,0.60/)

	var_name = (/"MSEDT","HMSEE","VMSEE","SFFLX","QRFLX","VDSEE"/)
	;var_name = (/"MSEDT","HMSEE","VMSEE","SFFLX","QRFLX"/)
    	num_vars = dimsizes(var_name)
	
	idir  = "/data/whannah/model/";"/local/"
	odir  = idir
	ifile = "MSE.budg.altZ.nc"
	ofile = "MSE.budg.altZ.pentad.nc"

	tspan = 5850
	tt = tspan-1


do v = 0,3
  infile = addfile(idir+vrun(v)+ifile,"r") 
  lat = infile->lat
  lon = infile->lon
  ;delete(infile)
  latsz = dimsizes(lat)
  lonsz = dimsizes(lon)
  ;V = new((/num_vars,tspan,latsz,lonsz/),"float")
  ;  V!0 = "var"
  ;  V!1 = "time"
  ;  V!2 = "lat"
  ;  V!3 = "lon"
  pV = new((/num_vars,tspan/5,latsz,lonsz/),"float")
  ;  fV!0 = "var"
  ;  fV!1 = "time"
  ;  fV!2 = "lat"
  ;  fV!3 = "lon"
  ;  fV&lat = lat
  ;  fV&lon = lon
  print("")
  
  do x = 0,num_vars-1
    print("creating pentad data for "+idir+vrun(v)+ifile+" > "+var_name(x))
    V = infile->$var_name(x)$(:tt,:,:) ;/ infile->VDSEE(:tt,:,:)
    pV(x,:,:,:) = ( V(0:tt-4:5,:,:)+V(1:tt-3:5,:,:)+V(2:tt-2:5,:,:)+V(3:tt-1:5,:,:)+V(4:tt-0:5,:,:) )/5.
      delete(V)
  end do 
  
  print("writing output file...")
  if isfilepresent(odir+vrun(v)+ofile) then
    system("rm "+odir+vrun(v)+ofile)
  end if
  outfile = addfile(odir+vrun(v)+ofile,"c")
  do x = 0,num_vars-1
    tmp = pV(x,:,:,:)
    tmp!0 = "time"
    tmp!1 = "lat"
    tmp!2 = "lon"
    tmp&lat = lat
    tmp&lon = lon
    outfile->$var_name(x)$ = tmp
    delete(tmp)
  end do
  delete(pV)
end do
    
    
    
    
    
    
    
end    
