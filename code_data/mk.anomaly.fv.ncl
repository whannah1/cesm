load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"

begin

	case = (/"raspe_01"/)

	ivar = (/"U","V","Q"/)

	lev = 850

	num_c = dimsizes(case)
  	num_v = dimsizes(ivar)

	dir = "/maloney-scratch/whannah/CESM/data/"
	
	m1 = 6		; start month
	d1 = 1		; start date
	
	m2 = 7		; end month
	d2 = 20		; end date
	
	day_beg = day_of_year(0,m1,d1)-2 -12
	day_end = day_of_year(0,m2,d2)-1 +12
	
	ovar = ivar
	
	ofile_stub = "anomaly"
	
	dt = 8		; time per sample (hr)
	
;=========================================================================================================
;=========================================================================================================
do c = 0,num_c-1
 do v = 0,num_v-1
  ;=========================================================================================================
  ; List files
  ;=========================================================================================================
  tfile = systemfunc("ls "+dir+case(c)+"/raw_data/"+case(c)+".cam2.h1.*")
  ifile = tfile(day_beg:day_end) 
  num_f = dimsizes(ifile)
  delete(tfile)  
  num_t = num_f*dt
  ;=========================================================================================================
  ; Create and initialize variables including coordinate variables
  ;=========================================================================================================
  infile = addfile(ifile(0),"r")
  sz = dimsizes(infile->$ivar(v)$)
  cnt = 0.
  lat = infile->lat
  lon = infile->lon 
  hya = infile->hyam
  hyb = infile->hybm
  num_d = dimsizes(sz)
  latsz = dimsizes(lat)
  lonsz = dimsizes(lon)
  if lev.eq.-1 then
    levs = infile->lev
    levsz = dimsizes(levs)
  else
    levs = lev
    levsz = dimsizes(lev)
  end if
    V = new((/num_t,levsz,latsz,lonsz/),float)
    V!0 = "time"
    V!1 = "lev"
    V!2 = "lat"
    V!3 = "lon"
    V&lev = levs
    V&lat  = lat
    V&lon  = lon
  ;else
  ;  V = new((/num_t,latsz,lonsz/),float)
  ;  V!0 = "time"
  ;  V!1 = "lat"
  ;  V!2 = "lon"
  ;  V&lat  = lat
  ;  V&lon  = lon
  ;end if
  V = 0. 
  ;=========================================================================================================
  ; Assign variable and output file names
  ;========================================================================================================= 
  print("")
  if num_d.eq.4 then
   if lev.eq.-1 then
    print("  case: "+case(c)+"  var: "+ivar(v)+" (all levels)")
    ofile = dir+case(c)+"/"+ofile_stub+"."+ovar(v)+"."+case(c)+".nc"
    V@long_name = ivar(v)
   else
    print("  case: "+case(c)+"  var: "+ivar(v)+lev)
    ofile = dir+case(c)+"/"+ofile_stub+"."+ovar(v)+lev+"."+case(c)+".nc"
    V@long_name = ivar(v)+lev
   end if
  end if
  if num_d.eq.3 then
   print("  case: "+case(c)+"  var: "+ivar(v))
   ofile = dir+case(c)+"/"+ofile_stub+"."+ovar(v)+"."+case(c)+".nc"
  end if
  ;=========================================================================================================
  ; Load data
  ;=========================================================================================================
  do f = 0,num_f-1
    infile = addfile(ifile(f),"r")
    ;--------------------------------------------------------------------------------
    if num_d.eq.4 then
     if lev.eq.-1 then
      V(f*dt:f*dt+dt-1,:,:,:) = infile->$ivar(v)$(:,:,:,:)
     else
      V(f*dt:f*dt+dt-1,:,:,:) = (/vinth2p(infile->$ivar(v)$,hya,hyb,lev,infile->PS,1,1000.,1,False)/)
     end if
    end if 
    ;--------------------------------------------------------------------------------
    if num_d.eq.3 then
      V(f,:,:) = infile->$ivar(v)$(:,:,:)
    end if
    ;--------------------------------------------------------------------------------
    cnt = cnt+1.
  end do
  ;=========================================================================================================
  ; Calculate anomaly
  ;=========================================================================================================
  ;if num_d.eq.4 then
    Vb = new((/num_t-dt,levsz,latsz,lonsz/),float)
    Vp = new((/num_t-dt,levsz,latsz,lonsz/),float)
  ;end if
  ;if num_d.eq.3 then
  ;  Vb = new((/num_t-24,latsz,lonsz/),float)
  ;  Vp = new((/num_t-24,latsz,lonsz/),float)    
  ;end if
  ;do t = 0,11
  ;  Vb(t,:,:) = dim_avg_n(V(0:11,:,:,:),0)
  ;  Vb(num_t-1-t,:,:) = dim_avg_n(V(num_t-1-12:num_t-1,:,:,:),0)
  ;end do
  do t = dt/2,num_t-1-dt/2
    Vb(t-dt/2,:,:,:) = dim_avg_n(V(t-dt/2:t+dt/2,:,:,:),0)
  end do
   temp = V(dt/2:num_t-1-dt/2,:,:,:)
   delete(V)
  V = temp
   delete(temp)
  Vp = V - Vb
   delete(V)
   if lev.eq.-1 then
     delete(Vb)
   end if
  ;=========================================================================================================
  ; Name dimensions
  ;=========================================================================================================
  print("--------------------------------------")
  ;printVarSummary("  "+Vp)
  ;printVarSummary(Vb)
  print("--------------------------------------")
   Vp!0 = "time"
   Vp!1 = "lev"
   Vp!2 = "lat"
   Vp!3 = "lon"
   Vp&lev = levs
   Vp&lat = lat
   Vp&lon = lon
   if lev.ne.-1 then
    Vb!0 = "time"
    Vb!1 = "lev"
    Vb!2 = "lat"
    Vb!3 = "lon"
    Vb&lev = levs
    Vb&lat = lat
    Vb&lon = lon
   end if
  ;=========================================================================================================
  ; test plot
  ;=========================================================================================================
    ;wks = gsn_open_wks("x11","test")
    ;res = True
    ;plot = gsn_csm_xy(wks,hr,V(:,32,54),res)
    ;exit
  ;=========================================================================================================
  ; Write output file
  ;=========================================================================================================
  print("    Writing output file...")
  if isfilepresent(ofile) then
    system("rm "+ofile)
  end if
  outfile = addfile(ofile,"c")
    outfile@start_date = m1+"/"+d1
    outfile@end_date   = m2+"/"+d2
  outfile->$(ovar(v)+"p")$ = Vp
  if lev.ne.-1 then
    outfile->$(ovar(v)+"b")$ = Vb
    delete(Vb)
  end if
  ;outfile->$ovar(v)$     = V
	delete([/infile,outfile,Vp,ofile/])
  ;=========================================================================================================
  ;=========================================================================================================
  delete(ifile)
 end do
end do
;=========================================================================================================
;=========================================================================================================
end

