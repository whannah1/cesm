load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_obs.ncl"
load "$NCARG_ROOT/custom_functions_CESM.ncl"

begin
	;case 	  = (/"raspe_eul_06","raspe_eul_07","raspe_eul_09","raspe_eul_10","raspe_eul_11","ERAi"/)
	case 	  = (/"raspe_eul_06"/)
	;case 	  = (/"ERAi"/)
	

	;case_name = (/"Control","DeepA","Tok","DeepB","DeepA+Tok"/)


	;season = "all_season"	
	;season = (/"all_season","winter","summer"/)

	num_c = dimsizes(case)

	dir = "/maloney-scratch/whannah/CESM/data/"+case+"/budget/"
	
	N_bdg = "S"
  	D_bdg = "Q"
	
	fig_type = "png"
	fig_file = "GMS.terms."+N_bdg+"."+D_bdg+"."+case;+".alt"
	
	lat1 = -20.
	lat2 =  20.
	
		Lv  	= 2.5*10.^6.	; J / kg
		To	= 273.		; K
  	
  	winter = True
  	
  	plot_name = (/"Total","Horz","Vert"/)
 
	
;====================================================================================================
;====================================================================================================
	
	res = True
	res@gsnDraw 		= False
	res@gsnFrame 		= False
	res@gsnAddCyclic 	= True
	res@gsnSpreadColors 	= True
	res@gsnRightStringFontHeightF  = 0.025
	res@gsnLeftStringFontHeightF   = 0.025
	;res@gsnCenterStringFontHeightF = 0.015
	res@mpCenterLonF 	= 180
	res@mpLimitMode 	= "LatLon"
	res@mpMinLatF 		= lat1
	res@mpMaxLatF 		= lat2
	res@tmXBLabelsOn 	= False
	res@tmXBLabelFontHeightF = 0.01
	res@tmYLLabelFontHeightF = 0.01
	res@cnLinesOn 		= False
	res@cnFillOn 		= True
	res@lbLabelBarOn 	= True
	if res@lbLabelBarOn then
	  res@lbLabelAngleF 		= -45.
	  res@lbLabelFontHeightF 	= 0.02
	  res@lbTopMarginF    		= 1.
	  res@lbBottomMarginF 		= 1.
	  res@lbTitleOffsetF  		= -.5
	  res@lbLabelOffsetF  		= -.5
	end if
	
  
		

;====================================================================================================
;====================================================================================================
	
	lat = LoadCESMlat("raspe_eul_06",lat1,lat2)
	lon = LoadCESMlon("raspe_eul_06",0.,360.)
	num_lat = dimsizes(lat)
	num_lon = dimsizes(lon)
	num_t = 10*365
	
  
	
  ;N_var_name = (/"del"+N_bdg+"V",N_bdg+"delV","Vdel"+N_bdg,"WdSdp"/)
  N_var_name = (/"del"+N_bdg+"V","Vdel"+N_bdg,"Wd"+N_bdg+"dp"/)
  D_var_name = (/"del"+D_bdg+"V","Vdel"+D_bdg,"Wd"+D_bdg+"dp"/)
  
  N_num_var = dimsizes(N_var_name)
  D_num_var = dimsizes(D_var_name)
  
  num_v = N_num_var+D_num_var
	
  N = new((/num_c,N_num_var,num_t,num_lat,num_lon/),float)
  D = new((/num_c,D_num_var,num_t,num_lat,num_lon/),float)
  
  
	
  do c = 0,num_c-1   
    if case(c).eq."ERAi" then
       yr1 = 0
       yr2 = 9
     else
       yr1 = 2
       yr2 = 8      
     end if 
    ;-------------------------------------------------------------
    ; Numerator
    ;-------------------------------------------------------------
    do v = 0,N_num_var-1
     ifile  = dir(c)+case(c)+"."+N_bdg+".budget."+N_var_name(v)+"."+yr1+"-"+yr2+".nc"
     infile = addfile(ifile,"r")
     num_t = dimsizes(infile->$N_var_name(v)$(:,0,0))
     ;tmp = area_hi2lores(infile->lon,infile->lat({lat1:lat2}),infile->$N_var_name(v)$(:,{lat1:lat2},:),True,1.,lon,lat,False)
     N(c,v,:num_t-1,:,:) = area_hi2lores(infile->lon,infile->lat({lat1:lat2}),infile->$N_var_name(v)$(:,{lat1:lat2},:),True,1.,lon,lat,False)
     ;N(c,v,:,:,:) = (/ infile->$N_var_name(v)$(:,{lat1:lat2},:) /)
    end do
    ;-------------------------------------------------------------
    ; Denominator
    ;-------------------------------------------------------------
    do v = 0,D_num_var-1
     ifile  = dir(c)+case(c)+"."+D_bdg+".budget."+D_var_name(v)+"."+yr1+"-"+yr2+".nc"
     infile = addfile(ifile,"r")
     num_t = dimsizes(infile->$D_var_name(v)$(:,0,0))
     D(c,v,:num_t-1,:,:) = area_hi2lores(infile->lon,infile->lat({lat1:lat2}),infile->$D_var_name(v)$(:,{lat1:lat2},:),True,1.,lon,lat,False)
     ;D(c,v,:,:,:) = (/ infile->$D_var_name(v)$(:,{lat1:lat2},:) /)
    end do
    ;-------------------------------------------------------------
    ;-------------------------------------------------------------
  end do


  	
  	N = runave_n_Wrap(N,17,0,2)
  	D = runave_n_Wrap(D,17,0,2)
  	
  	if D_bdg.eq."Q" then
  	  D = where(abs(D).lt.1e-5,D@_FillValue,D)
  	end if
  	
  ; Select Oct-Apr
  if winter then
  days_per_month = (/31,28,31,30,31,30,31,31,30,31,30,31/)
  do d = sum(days_per_month(:3)),sum(days_per_month(:8))-1
    days = d+ispan(0,9,1)*365
    N(:,:,days,:,:) = N@_FillValue
    D(:,:,days,:,:) = D@_FillValue
  end do
  end if


  	
  	
  NGMS = new((/num_c,N_num_var,num_lat,num_lon/),float)
   GMS = new((/num_c,N_num_var,num_lat,num_lon/),float)
   
   	NGMS!2 = "lat"
   	NGMS!3 = "lon"
   	NGMS&lat = lat
   	NGMS&lon = lon
   	
   	GMS!2 = "lat"
   	GMS!3 = "lon"
   	GMS&lat = lat
   	GMS&lon = lon
   	
  
  do v = 0,N_num_var-1
    NGMS(:,v,:,:) = dim_avg_n(-To*N(:,v,:,:,:)/(Lv*D(:,0,:,:,:)),1)
     GMS(:,v,:,:) = dim_avg_n( To*N(:,v,:,:,:),1)
  end do
  	
  
  aN = To*dim_avg_n_Wrap(N,2)
  aD = Lv*dim_avg_n_Wrap(D,2)
  
	  aN = smth9(aN,0.5,0.5,True)
	  aD = smth9(aD,0.5,0.5,True)
  
  
  		;if D_bdg.eq."Q" then
  		;  aD = aD*1000.
  		;end if
  
  
  	aN!0 = "case"
  	aN!1 = "var"
  	aN!2 = "lat"
  	aN!3 = "lon"
  	aN&var = N_var_name
  	aN&lat = lat
  	aN&lon = lon
  	
  	aD!0 = "case"
  	aD!1 = "var"
  	aD!2 = "lat"
  	aD!3 = "lon"
  	aD&var = D_var_name
  	aD&lat = lat
  	aD&lon = lon
  	
  	lat1 = -10.
  	lat2 =  10.
;====================================================================================================
;====================================================================================================
	if True then
	do c = 0,num_c-1
	  print("")
	  print("  case: "+case(c))
	  print("  ----------------------------------------------------------------------------------------")
	do v = 0,N_num_var-1
	  ;print("	-------------------------------------------------------------")
  	  print("	"+N_var_name(v)+":	min = "+min(aN(c,v,:,:))+"		max = "+max(aN(c,v,:,:))+"		avg = "+avg(aN(c,v,:,:)))
  	  ;print("	-------------------------------------------------------------")
  	end do
  	print("")
	do v = 0,D_num_var-1
	  ;print("	-------------------------------------------------------------")
  	  print("	"+D_var_name(v)+":	min = "+min(aD(c,v,:,:))+"		max = "+max(aD(c,v,:,:))+"		avg = "+avg(aD(c,v,:,:)))
  	  ;print("	-------------------------------------------------------------")
  	end do
  	end do
  	print("  ----------------------------------------------------------------------------------------")
  	print("")
	end if

;====================================================================================================
;====================================================================================================
do c = 0,num_c-1

  wks = gsn_open_wks(fig_type,fig_file(c))
  ;gsn_define_colormap(wks,"ViBlGrWhYeOrRe")
  gsn_define_colormap(wks,"ncl_default")    
  plot = new(num_v,graphic)
  
  	clat1 = -5.
  	clat2 =  5.
  	
  	res@lbLabelAutoStride = False
  	res@gsnRightString = case(c)
  	
  		if N_bdg.eq."S" then
		 res@cnLevelSelectionMode = "ManualLevels"
		 res@cnMinLevelValF  = -60.
		 res@cnMaxLevelValF  =  60.
		 res@cnLevelSpacingF =   5.
		end if
	
	res@cnLevelSelectionMode = "ExplicitLevels"
	res@cnLevels = (/-60.,-50.,-40.,-30.,-20.,0.,20.,30.,40.,50.,100./)

  do v = 0,N_num_var-1
    p = (v*2)
  	res@gsnLeftString = "GMS ("+plot_name(v)+")"
    plot(p) = gsn_csm_contour_map(wks,GMS(c,v,:,:),res)
  end do
  
	
  	res@cnLevelSelectionMode = "ExplicitLevels"
	res@cnLevels = (/-5.,-1.,-0.5,-0.2,-0.1,0.,0.1,0.2,0.5,1.,5./)
	
  do v = 0,D_num_var-1
    p = (v*2+1)
  	res@gsnLeftString = "NGMS ("+plot_name(v)+")"
    plot(p) = gsn_csm_contour_map(wks,NGMS(c,v,:,:),res)
  end do
  ;end do

;====================================================================================================
;====================================================================================================
  	pres = True
  	if res@lbLabelBarOn then
  	  pres@gsnPanelLabelBar      	= False
  	else
  	  pres@gsnPanelLabelBar      	= True
  	  pres@lbLabelAngleF 	   	= -45.
  	  pres@lbLabelFontHeightF    	= 0.01
	  pres@gsnPanelBottom		= 0.2
  	  pres@pmLabelBarOrthogonalPosF = -0.03
  	  pres@lbBoxMinorExtentF 	= 0.6
  	  pres@lbLeftMarginF		=  4.
  	  pres@lbRightMarginF		=  4.
  	end if
  	pres@gsnPanelFigureStrings 	= (/"a","b","c","d","e","f"/) ; add strings to panel
  	pres@amJust   		   	= "TopLeft"
  	
  	if winter then
  	  pres@txString	= "2000-2009 (Oct-Apr)"
  	else
  	  pres@txString	= "2000-2009 (Annual)"
  	end if
  	
	
	pres@gsnPanelFigureStringsFontHeightF	= 0.015
	pres@amJust   = "TopLeft"
  
  gsn_panel(wks,plot,(/num_v/2,2/),pres)

	print("")
	print(fig_file(c)+"."+fig_type)

    delete([/wks,plot/])

end do

;====================================================================================================
;====================================================================================================
  
end

