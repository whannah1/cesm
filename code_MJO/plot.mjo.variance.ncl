load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_CESM.ncl"

begin 

	case  = (/"raspe_eul_06","raspe_eul_09","raspe_eul_07","raspe_eul_11","raspe_eul_10"/)
	;case = (/"raspe_eul_07"/)

	idir = "/maloney-scratch/whannah/CESM/data/"+case+"/"
	
	var = "U"

	lev = 850.
	
	lat1 = -20.
	lat2 =  20.
	
	lon1 = 0.
	lon2 = 360.
	
	yr1 = 2
	yr2 = 8

	fig_type = "png"
	if isStrSubset("PREC",var) then
	  fig_file = "/maloney-scratch/whannah/CESM/mjo/mjo.variance."+case+"."+var
	else
	  fig_file = "/maloney-scratch/whannah/CESM/mjo/mjo.variance."+case+"."+var+"."+lev
	end if
	
	num_c = dimsizes(case)

;====================================================================================================
;====================================================================================================
do c = 0,num_c-1
	
	print("	case = "+case(c))
	
	;-------------------------------------------------------
	; load data
	;-------------------------------------------------------
	V = LoadCESMData(case(c),var,yr1,yr2,lev,lat1,lat2,lon1,lon2)
	
	if isStrSubset("PREC",var) then
	  V = V * 60.*60.*24.*1000.
	end if
	
	num_t = dimsizes(V(:,0,0))
	winter = new(num_t,logical)
	summer = new(num_t,logical)
	mkSeasonalIndex(num_t,winter,summer)
	
	;-------------------------------------------------------
	; Filter data
	;-------------------------------------------------------
	f1 = 20.
	f2 = 100.
	;f_lo = 1./30.
	;f_hi = 1./90.
	f_lo = 1./f1
	f_hi = 1./f2
	wgts = filwgts_lanczos(61,2,f_hi,f_lo,1.)
	tmp  = wgt_runave_n_Wrap(V,wgts,0,0)
	delete(V)
	V = tmp
	delete(tmp)
	;-------------------------------------------------------
	; Calculate variance
	;-------------------------------------------------------
	vv = dim_variance_n_Wrap(V(:,:,:),0)
	
	vals = ind(winter)
	vv_w = dim_variance_n_Wrap(V(vals,:,:),0)
	delete(vals)
	
	vals = ind(summer)
	vv_s = dim_variance_n_Wrap(V(vals,:,:),0)
	delete(vals)
	;-------------------------------------------------------
	; Plot variance
	;-------------------------------------------------------
	var_str = var+lev+" "+f1+"-"+f2+" day"
	wks = gsn_open_wks(fig_type,fig_file(c))
	gsn_define_colormap(wks,"WhiteBlueGreenYellowRed")
	
		res = True
		res@gsnDraw = False
		res@gsnFrame = False
		res@gsnRightStringFontHeightF   = .015
		res@gsnLeftStringFontHeightF    = .015
		res@gsnCenterStringFontHeightF  = .015
		res@gsnSpreadColors = True
		res@cnLinesOn = False
		res@cnFillOn = True
		res@lbLabelFontHeightF = 0.01
		res@mpMaxLatF = lat2
		res@mpMinLatF = lat1
		res@mpFillOn = False
		res@mpCenterLonF = 180.
		res@lbLabelAngleF = -45.
		res@gsnCenterString = case(c)
		res@gsnLeftString = var_str

		res@cnLevelSelectionMode = "ManualLevels"

		if var.eq."U" then
		 res@cnLevelSpacingF = 1.
		 res@cnMinLevelValF  = 1.
		 res@cnMaxLevelValF  = 15.
		end if
		if var.eq."Q" then
		 res@cnLevelSpacingF = 2e-7
		 res@cnMinLevelValF  = 2e-7
		 res@cnMaxLevelValF  = 20e-7
		end if

	num_p = 3

	plot = new(num_p,graphic)

	res@gsnRightString = "All Season"	
	plot(0) = gsn_csm_contour_map(wks,vv,res)
	res@gsnRightString = "Winter"
	plot(1) = gsn_csm_contour_map(wks,vv_w,res)
	res@gsnRightString = "Summer"
	plot(2) = gsn_csm_contour_map(wks,vv_s,res)

  gsn_panel(wks,plot,(/num_p,1/),False)

  delete([/wks,plot,res,V,winter,summer,vv,vv_w,vv_s/])
  
  	print("")
  	print(fig_file+"."+fig_type)
  	print("")

end do

end
	
	
