load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"


begin 

  

  dir = "/maloney-scratch/whannah/model/"

  vrun = dir+(/"cam31_ras_7/","cam31_ras_6/","cam31_ras_3/","cam31_ras_4/"/)
  mu   = (/0.0,0.6,0.2 ,0.2/)
  evap = (/0.3,0.3,0.05,0.6/)

  
  ;vrun = dir+(/"cam31_ras_7/","cam31_ras_6/"/)
  ;mu   = (/0.0,0.6/)
  ;evap = (/0.3,0.3/)
  ;wks = gsn_open_wks("pdf","/data/whannah/model/lag_comp/TEND.vi.hp.lag.comp.7_6.plot")
  
  ;vrun = dir+(/"cam31_ras_3/","cam31_ras_4/"/)
  ;mu   = (/0.2 ,0.2/)
  ;evap = (/0.05,0.6/)
  
  
  ;wks = gsn_open_wks("x11","/data/whannah/model/lag_comp/TEND.vi.hp.lag.comp.3_4.plot")
  
  
  v1 = 0
  v2 = 3
  
    ifile = (/"DSE.budg.nc","Lq.budg.nc"/)

    ;var_name = (/"VMSEE","HMSEE","SFFLX","QRFLX","MSEDT","VDSEE"/)
    ;var_name = (/"VMSEE","HMSEE","SFFLX","QRFLX","MSEDT"/)
    ;var_name = (/"VMSEE","MSEDT"/)
    ;var_name = (/"DSEDT","HDSEE","VDSEE","SHFLX","PRECP"/)
    ;var_name = (/"LQDT","HLQE","VLQE","LQPCP","RES"/)
    
	;color = (/1,225,238,129,90,187/)
	;color = (/90,225,1,187,195/)
	color = (/90,225,1,238,187/)
	
	ymax =  10
	ymin = -10
	
	
	lat1 = 4
	lat2 = 9


  do f = 0,1
  do v = v1,v2
    infile = addfile(vrun(v)+ifile(f),"r")
    if f.eq.0 then
      var_name = (/"DSEDT","HDSEE","VDSEE","SHFLX","PRECP"/)
      print("")
      print(""+(infile->lat(lat1))+" : "+(infile->lat(lat2)) )
      print("")
    end if
    if f.eq.1 then
      delete(var_name)
      var_name = (/"LQDT","HLQE","VLQE","PRECP","RES"/)
    end if
    num_vars = dimsizes(var_name)
    print("")
    print("  "+vrun(v)+ifile(f))
    print("")
    do i = 0,num_vars-1
    
      print("    "+var_name(i)+"  "+avg(infile->$var_name(i)$(:,lat1:lat2,:)) )
      
      ;print("    "+(infile->lat))
      ;exit
    
  
    end do
  end do
  end do
  
  
  
  
  
  
  
  
end


	; 	Lat
	;(0)	    -18.139
	;(1)	    -15.3484
	;(2)	    -12.5578
	;(3)	    -9.76715
	;(4)	    -6.97653
	;(5)	    -4.18592
	;(6)	    -1.39531
	;(7)	    1.39531
	;(8)	    4.18592
	;(9)	    6.97653
	;(10)	    9.76715
	;(11)	    12.5578
	;(12)	    15.3484
	;(13)	    18.139

