load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
begin
	idir = "/Users/whannah/CESM/code_PD/"
	
	;case = "SPCESM4xCO2"
	case = (/"SPCESMCTRL","SPCESM4xCO2"/)
	
	lev = 700.
	
	fig_type = "png"
	
	
	lat1 = -20.
	lat2 =  30.
	lon1 =   0.
	lon2 = 360.
	
	plat1 = -10.
	plat2 =  29.
	plon1 = -50.
	plon2 =  50.
	
	reg = "east"
	
	if reg.eq."west" then
	  clat1 =  5. - 2.
	  clat2 = 15. - 2.
	  clon1 =  0.
	  clon2 = 10.
	end if
	if reg.eq."east" then
	  clat1 =   5. + 2.
	  clat2 =  15. + 2.
	  clon1 = -15. + 360.
	  clon2 =  -5. + 360.
	end if
	
	ndayrun = 11
	
	title = ""
	
	fig_file = idir+"AEW.reg_comp."+reg+".v1"
	
;===================================================================================
;===================================================================================
  num_c = dimsizes(case)
  do c = 0,num_c-1
	ifile1 = idir+case(c)+".PRECT.nc"
	ifile3 = idir+case(c)+".U.nc"
	ifile4 = idir+case(c)+".V.nc"
	infile1 = addfile(ifile1,"r")
	infile3 = addfile(ifile3,"r")
	infile4 = addfile(ifile4,"r")
	if c.eq.0 then
	  lat = infile1->lat({lat1:lat2})
	  lon = infile1->lon({lon1:lon2})
	  nt    = dimsizes(infile1->time)
	  latsz = dimsizes(lat)
	  lonsz = dimsizes(lon)
	  xdim = (/num_c,nt,latsz,lonsz/)
	  P = new(xdim,float)
	  EKE = new(xdim,float)
	  EVR = new(xdim,float)
	  U   = new(xdim,float)
	  V   = new(xdim,float)
	  Uj  = new(xdim,float)
	end if
	P(c,:,:,:)  = infile1->PRECT(:,{lat1:lat2},{lon1:lon2})*1000.*24.*3600.
	Uj(c,:,:,:) = infile3->U(:,{600.},{lat1:lat2},{lon1:lon2})
	U(c,:,:,:)  = infile3->U(:,{lev},{lat1:lat2},{lon1:lon2})
	V(c,:,:,:)  = infile4->V(:,{lev},{lat1:lat2},{lon1:lon2})
	
	Up = U(c,:,:,:) - runave_n_Wrap(U(c,:,:,:),ndayrun,0,0)
	Vp = V(c,:,:,:) - runave_n_Wrap(V(c,:,:,:),ndayrun,0,0)
		
		xvals = ispan(0,lonsz-1,1)
        dxv = xvals(1) - xvals(0)
        Rvals = where((xvals+1).le.max(xvals),xvals+1,xvals+1-(max(xvals)+1))
        Xvals = xvals
        Lvals = where((xvals-1).ge.min(xvals),xvals-1,xvals-1+(max(xvals)+1))
		yvals = ispan(0,latsz-1,1)
		Nvals = yvals(2:latsz-1)
		Cvals = yvals(1:latsz-2)
		Svals = yvals(0:latsz-3)
		dUpdy = new(dimsizes(Up),float)
		dVpdx = new(dimsizes(Up),float)
		dlon = lon(1)-lon(0)
		dx = conform(Up(:,Cvals,Xvals), tofloat( (lon+dlon+lon(Xvals))/2. 		\
		                                        -(lon-dlon+lon(Xvals))/2. ) ,2)
        dy = conform(Up(:,Cvals,Xvals), tofloat( (lat(Nvals)+lat(Cvals))/2. 		\
                                                -(lat(Svals)+lat(Cvals))/2. ) ,1)
        dx = dx*111000.*conform(dUpdy(:,Cvals,Xvals),tofloat(cos(lat(Cvals)*3.14159/180.)),1)
        dy = dy*111000.        
    dUpdy(:,Cvals,Xvals) = ( (Up(:,Nvals,Xvals)+Up(:,Cvals,Xvals))/2.  \
                            -(Up(:,Svals,Xvals)+Up(:,Cvals,Xvals))/2.   )/dy
	dVpdx(:,Cvals,Xvals) = ( (Vp(:,Cvals,Rvals)+Vp(:,Cvals,Xvals))/2.  \
                            -(Vp(:,Cvals,Lvals)+Vp(:,Cvals,Xvals))/2.   )/dx
	
	EKE(c,:,:,:) = ( Up^2. + Vp^2. )/2.
	
	EVR(c,:,:,:) = ( dVpdx - dUpdy )
	
	delete([/Up,Vp/])
	delete([/dx,dy,dUpdy,dVpdx/])
  end do
	
	
	P@long_name 		= "PRECT"
	Uj@long_name 	= "600mb Zonal Wind"
	EKE@long_name 	= lev+"mb Eddy Kinetic Energy ("+ndayrun+" day mean pert.)"
	EVR@long_name 	= lev+"mb Eddy Vorticity ("+ndayrun+" day mean pert.)"
	
	P@units 		= "mm/day"
	EKE@units 	= "m~S~2~N~ s~S~-2~N~"
	EVR@units 	= ""
	
	P!0 = "case"
	P!1 = "time"
	P!2 = "lat"
	P!3 = "lon"
	P&lat = lat
	P&lon = lon
	copy_VarCoords(P,EKE)
	copy_VarCoords(P,EVR)
	copy_VarCoords(P,Uj)
;===================================================================================
;===================================================================================
	index = dim_avg_n_Wrap(EVR(:,:,{clat1:clat2},{clon1:clon2}),(/2,3/))
	
	comp = new((/num_c,3,latsz,lonsz/),float)
	
	;threshold = 1.5*stddev(index)
	;condition = conform(EKE(:,1:nt-2,:,:),\
	;            (index(:,1:nt-2).gt.threshold) 			.and.\
	;		    (index(:,0:nt-3).lt.index(:,1:nt-2))		.and.\
	;		    (index(:,2:nt-1).lt.index(:,1:nt-2))   	,(/0,1/))
	;		    
	;num_cond = dim_num_n(condition,(/1/))
	
  ;comp(:,0,:,:) = dim_avg_n( where(condition,EVR(:,1:nt-2,:,:),EVR@_FillValue) ,(/1/))
  
  do c = 0,num_c-1
    comp(c,0,:,:) = regCoef(index(c,:)/stddev(index(c,:)),EVR(case|c,lat|:,lon|:,time|:))
  end do
  
  	comp!0 = "case"
  	comp!1 = "lag"
  	comp!2 = "lat"
  	comp!3 = "lon"
  	comp&lat = lat
  	comp&lon = lon
;===================================================================================
;===================================================================================
  wks  = gsn_open_wks(fig_type,fig_file)
  gsn_define_colormap(wks,"WhiteBlueGreenYellowRed")
  plot = new(2*num_c,graphic)
	  res = True
	  res@gsnDraw 			= False
	  res@gsnFrame			= False
	  res@gsnAddCyclic		= True
	  res@gsnSpreadColors 	= True
	  res@cnFillOn			= True
	  res@cnLinesOn			= False
	  res@mpCenterLonF 		= 0.
	  res@mpMinLonF			= plon1
	  res@mpMaxLonF			= plon2
	  res@mpMinLatF 			= plat1
	  res@mpMaxLatF			= plat2
	  
	  res@cnLevelSelectionMode = "ExplicitLevels"
	  
	  res1 = res
	  res2 = res
	  
	  res1@gsnLeftString = lev+" mb Eddy Vorticity Variance"
	  res2@gsnLeftString = lev+" mb Eddy Vort. composite"
	  
	  ;if ndayrun.eq.5 then	  
	  ;end if
	  if ndayrun.eq.10 then
	   res1@cnLevels = ispan(8,24,2)*1e-11		; EVR variance
	  end if
	
	if reg.eq."east" then res2@cnLevels = ispan(-60,150,30)*1e-7 end if
	if reg.eq."west" then res2@cnLevels = ispan(-20,80,10)*1e-7 end if
	
print(min(comp))	  
print(max(comp))
	  
  do c = 0,num_c-1
  		res1@gsnRightString = case(c)
  		res2@gsnRightString = case(c)
    plot(num_c*0+c) = gsn_csm_contour_map(wks,dim_variance_n_Wrap(EVR(c,:,:,:),0),res1)
    plot(num_c*1+c) = gsn_csm_contour_map(wks,comp(c,0,:,:),res2)
  end do
  
  gres                 	= True
  gres@gsLineColor		= "black"
  gres@gsLineThicknessF = 2.0

  blat = (/clat1,clat2,clat2,clat1,clat1/)
  blon = (/clon1,clon1,clon2,clon2,clon1/)
  dum = new(dimsizes(plot),graphic)
  do c = 0,num_c-1
    dum(num_c*0+c) = gsn_add_polyline(wks,plot(num_c*0+c),blon,blat,gres)
    dum(num_c*1+c) = gsn_add_polyline(wks,plot(num_c*1+c),blon,blat,gres)
  end do
  

  	pres = True
  	pres@txString = title
  	
  gsn_panel(wks,plot,(/2,num_c/),pres)
  
  print("")
  print("  "+fig_file+"."+fig_type)
  print("")

;===================================================================================
;===================================================================================
end
