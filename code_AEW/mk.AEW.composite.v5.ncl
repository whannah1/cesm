; an index of 2-10 day V-wind is used to calculate 
; 3D datasets of lag-composited variables 
; v1 - adapted from code_AEW/mk.AEW.regression.v1.ncl
;      add "_filt" to a variable to composite 2-10 filtered version
;      events are selected when the filtered V-wind > threshold
; v2 - similar to v1, but events are selected based on a narrow range
;      instead of a lower boundary like in v1
;      i.e. threshold <= (threshold-tolerance) < V_event < (threshold+tolerance)
; v3 - identical to v2 - just used for computing weak and strong composites simultaneously
; v4 - based on v2 - index based on eddy vorticity - weak waves
; v5 - same as v4 - strong waves
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_CESM.ncl"
load "$NCARG_ROOT/custom_functions_TRMM.ncl"
begin
    
    debug = False

    if .not.isvar("case") then
        case = "CESM_SP_CTL"
        ; case = "CESM_SP_EXP"
        ; case = "ERAi"
    end if
    if .not.isvar("area") then area = 7 end if

    ndays = 2
    threshold = 16.
    tolerance = 4.
    fac = 1.;e6

    if case.eq."ERAi" then ilev = 600 else ilev = 500 end if
    
    ; var = (/"EVR","V_filt","Q1_filt","OMEGA_filt"/)
    ; var = "UbdEpdx"
    ; var = "VbdEpdy"

    ; var = (/"U","V"/)
    var = (/"Q1","Q","Q_filt"/)
    ; var = (/"dUbdp","dUpdp","dWpdy","dWbdy"/)

    ; var := array_append_record(var,(/"UbdEpdx","UpZpdxZb","UpZpdxZp"/),0)
    ; var := array_append_record(var,(/"VbdEpdy","VpZpdyZb","VpZpdyZp"/),0)
    ; var := array_append_record(var,(/"UpZpdxZb","UpZpdxZp"/),0)
    ; var := array_append_record(var,(/"VpZpdyZb","VpZpdyZp"/),0)
    ; var := array_append_record(var,(/"WbdEpdp","WpZpdpZb","WpZpdpZp"/),0)
    ; var := array_append_record(var,(/"EpdpWb" ,"ZpZbdpWp" ,"ZpZpdpWp"/),0)
    ; var := array_append_record(var,(/"Zp_dpVp_dxWb","Zp_dpVb_dxWp","Zp_dpUp_dyWb","Zp_dpUb_dyWp","Zp_dpVp_dxWp","Zp_dpUp_dyWp"/),0)
    
    ; var := array_append_record(var,(/"Ap_Bc","Ap_Qp"/),0)
    ; var := array_append_record(var,(/"OMEGA","T","Q","Z"/),0)
    ; var := array_append_record(var,(/"PPV"/),0)
    ; var := array_append_record(var,(/"UBdPPVdx", "UPdBPVdx", "VBdPPVdx", "VPdBPVdx"/),0)
    ; var := array_append_record(var,"PPVQ"+(/1,2,3,4/),0)
    ; var := array_append_record(var,(/"PV","PVQ","WdPVdp","UdPVdx","VdPVdy"/),0)
    ; var = "PPV"+(/1,2,3/)
    ; var = "PPVQ"+(/1,2,3,4/)
    ; var = "PPVCMT"+(/1,2,3/)

    daily_avg = False
;==========================================================================
;==========================================================================
    case = CESM_get_case(case)

    casename = CESM_get_casename(case)
    if .not.isvar("casename") then casename = case end if

    if daily_avg then nh = 1 else nh = 4 end if

    mxlag = ndays*nh

    lag = tofloat( ispan(-mxlag,mxlag,1) ) / tofloat(nh)
    
    dt = 24/nh*3600.

    dx = 5
    clat = 10
    if area.eq.0 then clon =  15 end if
    if area.eq.1 then clon =  10 end if
    if area.eq.2 then clon =  05 end if
    if area.eq.3 then clon =  00 end if
    if area.eq.4 then clon = -05 end if
    if area.eq.5 then clon = -10 end if
    if area.eq.6 then clon = -15 end if
    if area.eq.7 then clon = -20 end if

    ilat1 =  clat - dx
    ilat2 =  clat + dx
    ilon1 =  clon - dx 
    ilon2 =  clon + dx

    CESM_trunclen  = 10
    if debug then CESM_trunclen = 1 end if

    opt  = True
    opt@ERAIyr1    = 2000
    opt@ERAIyr2    = opt@ERAIyr1 + CESM_trunclen-1
    opt@TRMMyr1    = opt@ERAIyr1
    opt@TRMMyr2    = opt@ERAIyr2
    opt@debug      = debug
    opt@truncate   = True
    opt@use6h      = True
    ;lev = CESM_default_lev({200:1000})
    if case.eq."ERAi" then lev = ERAI_default_lev end if
    if case.ne."ERAi" then lev = CESM_default_lev end if
    opt@lev   = -1
    ;opt@lev = lev

    ; North Africa coords
    opt@lon1  = -40.
    opt@lon2  =  40.
    opt@lat1  = -20.
    opt@lat2  =  40.

    fca = 1./(10.*nh)   ; longer period
    fcb = 1./( 2.*nh)   ; shorter period
    opt = False       

    nwgt  = 14*nh+1
    sigma = 1.0
    wgt   = filwgts_lanczos (nwgt,2,fca,fcb,sigma)      ; band pass
    ;wgt   = filwgts_lanczos (nwgt,1,fca,fcb,sigma)      ; high pass
;==========================================================================
; Create 2-10 day Index
;==========================================================================
c = 0
    iopt = opt
    delete(iopt@lev)
    iopt@lev = ilev
    V := LoadCESM(case(c),"EVR",iopt)

    if daily_avg then 
        tmp := block_avg(V,4) 
        V := tmp
    end if
    time = V&time
    lat  = V&lat
    lon  = V&lon
    nlev = dimsizes(lev)
    nlat = dimsizes(lat)
    nlon = dimsizes(lon)

    Vp := wgt_runave_n_Wrap(V,wgt,0,0)

    index := dim_avg_n_Wrap( Vp(:,{ilat1:ilat2},{ilon1:ilon2}),(/1,2/))

    mn := index@mn
    if daily_avg then mn := index@mn(::4) end if
    condition := mn.eq.7 .or. mn.eq.8 .or. mn.eq.9 ;.or. mn.eq.10
    vals := where(condition,ispan(0,dimsizes(mn)-1,1),-1)
    if .not.all(ismissing(vals)) then 
        vals  := vals(ind(vals.ge.0)) 
    else
        print("ERROR: 'vals' contains missing values!")
        exit
    end if

    tmp   := time(vals)
    time  := tmp

    tmp   := index(vals)
    index := tmp

    ;index = (/ index - avg(index) /)
    ;index = (/ index / stddev(index) /)

    index!0 = "time"
    index&time = time
    if isatt(index,"mn") then delete(index@mn) end if
    if isatt(index,"yr") then delete(index@yr) end if

    nt = dimsizes(index)

    ; threshold = 1.*stddev(index)
    ; threshold = 2.
    condition := new(dimsizes(index),logical)
    condition(1:nt-2) = (index(1:nt-2).ge.(threshold - tolerance)*fac)     .and.\
                        (index(1:nt-2).lt.(threshold + tolerance)*fac)     .and.\
                        (index(0:nt-3).lt.index(1:nt-2)) .and.\
                        (index(2:nt-1).lt.index(1:nt-2))
    ;condition := conform(K(1:nt-2,:,:),cond,(/0/))

    if .not.debug then 
        ofile_stub = "~/Data/CESM/"+case+"/data/AEW.composite.v4"
        if daily_avg then ofile_stub = ofile_stub+".daily" end if
        ofile_stub = ofile_stub+".a"+area+".th_"+sprintf("%2.1f",threshold)

        ofile = ofile_stub+".index.nc"
        if fileexists(ofile) then system("rm "+ofile) end if
        outfile = addfile(ofile,"c")
        outfile->index = index
        outfile->mxlag = mxlag
        outfile->ilat1 = ilat1
        outfile->ilat2 = ilat2
        outfile->ilon1 = ilon1
        outfile->ilon2 = ilon2

        outfile->threshold = threshold
        outfile->tolerance = tolerance
        outfile->threshold_fac = fac
        print("")
        print("  "+ofile)
        print("")
    end if

    delete([/V,Vp,tmp/])
;==========================================================================
; Load data and calculate lag-composited field (vertically integrate later)
;==========================================================================
    num_v = dimsizes(var)
    do v = 0,num_v-1
        ;--------------------------------------
        ; Load variable
        ;--------------------------------------
        tvar = var(v)
        if var(v).eq."Z" then tvar = "Z3" end if
        if isStrSubset(var(v),"_filt") then tvar = str_sub_str(var(v),"_filt","") end if
        ; iX = tofloat( LoadCESM(case(c),tvar,opt) )
        iX = LoadCESM(case(c),tvar,opt)
        if typeof(iX).ne."float" then 
            tmp = tofloat(iX)
            copy_VarCoords(iX,tmp)
            iX := tmp
            delete(tmp)
        end if
        if var(v).eq."Z" .and. case(c).ne."ERAi" then iX = (/iX*g/) end if

        lat  := iX&lat
        lon  := iX&lon
        ;--------------------------------------
        ; filter 2-10 days - if needed
        ;--------------------------------------
        if isStrSubset(var(v),"_filt") then
            tmp := wgt_runave_n_Wrap(iX,wgt,0,0)
            iX  := tmp
            delete(tmp)
        end if
        ;--------------------------------------
        ; Daily average - if needed
        ;--------------------------------------
        if daily_avg then 
            tmp := block_avg(iX,4) 
            iX  := tmp
        end if
        ;--------------------------------------
        ; isolate season
        ;--------------------------------------
        dims := dimsizes(iX)
        ndim := dimsizes(dims)
        if ndim.eq.4 then X := iX(vals,:,:,:) end if
        if ndim.eq.3 then X := iX(vals,:,:) end if
        delete(iX)
        ;--------------------------------------
        ; initialize composite variable
        ;--------------------------------------
        dims := dimsizes(X)
        ; if ndim.eq.4 then xdim := (/mxlag*2+1,nlev,nlat,nlon/) end if
        ; if ndim.eq.3 then xdim := (/mxlag*2+1     ,nlat,nlon/) end if    
        if ndim.eq.4 then xdim := (/mxlag*2+1,dims(1),dims(2),dims(3)/) end if
        if ndim.eq.3 then xdim := (/mxlag*2+1        ,dims(1),dims(2)/) end if    
        COMP := new(xdim,float)
        STDV := new(xdim,float)
        if ndim.eq.4 then 
            COMP!0 = "lag"
            COMP!1 = "lev"
            COMP!2 = "lat"
            COMP!3 = "lon"
            COMP&lev = lev
        end if
        if ndim.eq.3 then 
            COMP!0 = "lag"
            COMP!1 = "lat"
            COMP!2 = "lon"
        end if
        COMP&lag = lag
        COMP&lat = lat
        COMP&lon = lon
        copy_VarCoords(COMP,STDV)
        ;--------------------------------------
        ; make lagged composite
        ;--------------------------------------
        do l = -mxlag,mxlag
            lvals = ispan(mxlag,nt-mxlag-1,1)
            ll = l+mxlag
            if ndim.eq.4 then 
                tcond := conform(X(lvals,:,:,:),condition(lvals),(/0/))
                COMP(ll,:,:,:) = dim_avg_n   ( where(tcond,X(lvals+l,:,:,:),X@_FillValue) ,0)
                STDV(ll,:,:,:) = dim_stddev_n( where(tcond,X(lvals+l,:,:,:),X@_FillValue) ,0)
            end if
            if ndim.eq.3 then 
                tcond := conform(X(lvals,:,:),condition(lvals),(/0/))
                COMP(ll,:,:) = dim_avg_n   ( where(tcond,X(lvals+l,:,:),X@_FillValue) ,0)
                STDV(ll,:,:) = dim_stddev_n( where(tcond,X(lvals+l,:,:),X@_FillValue) ,0) 
            end if
            delete(lvals)
        end do
        ;--------------------------------------
        ;--------------------------------------
        STDV@long_name = "Standard deviation of "+var(v)
        if isatt(COMP,"mn") then delete(COMP@mn) end if
        if isatt(COMP,"yr") then delete(COMP@yr) end if
        if .not.debug then 
            ; ofile = "~/Data/CESM/"+case+"/data/AEW.composite."
            ; if daily_avg then ofile = ofile+"daily." end if
            ; ofile = ofile+"v"+area+"."+var(v)+".nc"
            ofile = ofile_stub+"."+var(v)+".nc"
            if fileexists(ofile) then system("rm "+ofile) end if
            outfile = addfile(ofile,"c")
            outfile->$var(v)$ = COMP 
            outfile->STDV     = STDV
        end if
        ;--------------------------------------
        ;--------------------------------------
        delete([/X,COMP,STDV/])
        ;--------------------------------------
        ;--------------------------------------
        print("")
        print("    "+ofile)
    end do 
    print("")
;==========================================================================
;==========================================================================
end
