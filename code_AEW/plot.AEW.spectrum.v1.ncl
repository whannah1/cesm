; Plot a map of mean quantities related to AEW activity
; v1 - old version of data loading strategy
; v2 - new version with adaptable modular LoadCESM() method
; v3 - same as v2, except variables are vertically integrated
; v4 - similar to v2, plots std. dev. a 2-10 day filtered variable (i.e. precip)
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_CESM.ncl"
begin

    debug = False

    idir = "~/Research/CESM/figs_AEW/"

    case = (/"ERAi","CESM_SP_CTL","CESM_SP_EXP","CESM_ZM_CTL","CESM_ZM_EXP"/)
    ; case = (/"CESM_SP_CTL","CESM_SP_EXP","CESM_ZM_CTL","CESM_ZM_EXP"/)
    ; case = (/"CESM_ZM_CTL","CESM_ZM_EXP"/)
    ; case = (/"CESM_SP_CTL","CESM_SP_EXP"/)
    ; case = "ERAi"
    
    var = "PEN"
    lev = 500
    

    recalc = True

    
    clon =  -20
    lon1 = -5. + clon
    lon2 =  5. + clon
    lat1 =  5.
    lat2 = 15.

    fig_type = "png"
    fig_file = idir+"AEW.spectrum.v1"

    months = (/6,7,8,9,10/)
    season = "JAS"
    ; months = (/8/)
    ; season = "Aug"

;===================================================================================
;===================================================================================
    case = CESM_get_case(case)
    casename = CESM_get_casename(case)
    num_c = dimsizes(case)
    num_v = dimsizes(var)

    
    if debug then CESM_trunclen = 1 else CESM_trunclen = 8 end if

    opt  = True
    opt@ERAIyr1   = 2000
    opt@ERAIyr2   = 2009
    opt@debug    = False
    opt@truncate = True
    opt@monthly  = False
    opt@use6h    = True

    opt@lat1  = lat1
    opt@lat2  = lat2
    opt@lon1  = lon1
    opt@lon2  = lon2

    wks  = gsn_open_wks(fig_type,fig_file)
    plot = new(num_v*num_c,graphic)
        res = setres_default()

        res@trXMinF = 1./20.


;===================================================================================
;===================================================================================
do c = 0,num_c-1
do v = 0,num_v-1
    tfile = "~/Data/CESM/"+case(c)+"/"+case(c)+".spectrum.v1.clon_"+sprinti("%2.2i",clon)+"."+var(v)+".nc"
    printline()
    print("case: "+case(c)+"     "+tfile)

    opt@lev = lev
    if case(c).eq."ERAi" then opt@lev = 600 end if
    ;-----------------------------------------------
    ; Load data and select season
    ;-----------------------------------------------
    if recalc then 
        ;----------------------------------
        ;----------------------------------
        iX := dim_avg_n_Wrap( LoadCESM(case(c),var(v),opt) ,(/1,2/))
        ;----------------------------------
        ;----------------------------------
        if v.eq.0 then 
            mn := iX@mn
            condition := mn.eq.0
            do m = 0,dimsizes(months)-1 condition = condition.or.(mn.eq.months(m)) end do
            vals := where(condition,ispan(0,dimsizes(mn)-1,1),-1)
            vals:= vals(ind(vals.ge.0))

            yr := block_avg( iX@yr(vals) ,4)
            unique_years := get_unique_values(yr)
            nyr = dimsizes(unique_years)
        end if
        X := block_avg( iX(vals) ,4)
        ;----------------------------------
        ;----------------------------------
        do y = 0,nyr-1
            tX = X(ind(yr.eq.unique_years(y)))
            tmp = specx_anal(tX,1,3,0.2) 
            if y.eq.0 then 
                frq  := tmp@frq 
                spec := new((/nyr,dimsizes(tmp@spcx)/),float)
                spec = 0.
                lag1sum := 0.
            end if
            spec(y,:) = tmp@spcx
            r1 = tmp@xlag1
            lag1sum = lag1sum + 0.5*log((1+r1)/(1-r1))
        end do
        lag1 := lag1sum / nyr
        lag1 = (exp(2.*lag1)-1.)/(exp(2.*lag1)+1.)
        ;----------------------------------
        ;----------------------------------
        if fileexists(tfile) then system("rm "+tfile) end if
        outfile = addfile(tfile,"c")
        outfile->spec = spec
        outfile->lag1 = lag1
        outfile->frq  = frq
        outfile->nyr  = nyr
    else
        infile = addfile(tfile,"r")
        spec := infile->spec
        lag1 := infile->lag1
        frq  := infile->frq
        nyr  := infile->nyr
    end if
    ;-----------------------------------------------
    ;-----------------------------------------------
    spec_sum = dim_sum_n(spec,0) / nyr

    df := 2.*nyr
    df@spcx  = spec_sum
    df@frq   = frq
    df@xlag1 = lag1

    spec_ci = specx_ci(df,0.05,0.95)
    ;-----------------------------------------------
    ;-----------------------------------------------
        tres := res
        tres@gsnCenterString   = season
        tres@gsnRightString    = opt@lev+"mb "+var(v)+" @ "+clon+"E"
        tres@gsnLeftString     = casename(c)
        tres@xyLineThicknesses = (/6.,1,1,1/)
        tres@xyDashPatterns    = (/0,0,1,1/)
        tres@tmXBMode   = "Explicit"
        tm_periods      = (/2,3,4,6,8,10,20/)
        tres@tmXBLabels = tm_periods
        tres@tmXBValues = 1./tm_periods

        if var(v).eq."V"     then tres@trYMaxF =  40. end if
        if var(v).eq."EVR"   then tres@trYMaxF = 240. end if
        if var(v).eq."PRECT" then tres@trYMaxF = 200. end if
    
    i = v*num_c + c
    ; i = c*num_v + v

    plot(i) = gsn_csm_xy(wks,frq,spec_ci,tres)

    ;-----------------------------------------------
    ;-----------------------------------------------
end do
end do
printline()
;===================================================================================
;===================================================================================
        pres = setres_panel_labeled()
        pres@gsnPanelFigureStringsFontHeightF    = 0.01


    layout = (/num_v,num_c/)
    ; layout = (/num_c,num_v/)

    gsn_panel(wks,plot,layout,pres)

    trimPNG(fig_file)
;===================================================================================
;===================================================================================
end
